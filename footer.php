  
  <?php 
  $phone1 = get_option( "theme_option_contact_phone_1" );
  $phone2 = get_option( "theme_option_contact_phone_2" );
  ?>

  <!-- footer -->
   <div id="footer">
    <div class="wrapper">
      <div class="container">
        <div class="logo">
          <a href="">
            <img alt="logo" src="<?php echo REL_ASSETS_URI; ?>images/logo_black.png">
          </a>
        </div>
        <div class="footer_menu">
          <ul>
            <li>
              <a href="about">
                О нас
              </a>
            </li>
            <li>
              <a href="blog">
                Блог
              </a>
            </li>
          </ul>
          <ul>
            <li>
              <a href="promo">
                Акции
              </a>
            </li>
            <li>
              <a href="contacts">
                Контакты
              </a>
            </li>
          </ul>
        </div>
        <div class="footer_info">
          <div class="phones">
            <p>
              <?= $phone1; ?>
            </p>
            <p>
              <?= $phone2; ?>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end footer -->

  <div class="modal_wrapper close">
    <div class="modal_window">
      <div class="close_window">
        <span></span>
        <span></span>
      </div>
      <p>Оформить заказ в один клик</p>
      <form class="one_click_form">
        <input type="text" name="user_name" placeholder="Ваше имя" autocomplete="off">
        <input type="tel"  name="user_phone" placeholder="Ваш телефон" required>
        <input type="hidden" name="item_id" class="item_id__field" value="">
        <input type="hidden" name="action" value="one_click_form">
        <input type="submit" value="Заказать звонок">
      </form>
    </div>
  </div>

  <!-- script -->
  <?php wp_footer(); ?>
  <!-- end script -->
  
  </body>
</html>
