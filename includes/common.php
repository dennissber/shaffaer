<?php

/*
* Common for admin
*/
 
class Common {
	function __construct( ) {
		add_action( 'admin_enqueue_scripts', array($this,'init_scripts') );
    // save_post
    add_action( 'save_post', array($this,'meta_fields_update'));
	}
	
	
	## add script to admin panel
	public function init_scripts() {
    if ( ! did_action( 'wp_enqueue_media' ) ){ wp_enqueue_media(); }

    wp_enqueue_script( 'wps_select2', INC_URI.'assets/select2/js/select2.full.min.js', array('jquery'), null, false );
    wp_enqueue_style ( 'wps_select2',  INC_URI.'assets/select2/css/select2.min.css' );

		wp_enqueue_script( 'wps_admin_script', INC_URI.'assets/admin.script.js', array('jquery'), null, false );
    wp_enqueue_style ( 'wps_admin_style', INC_URI.'assets/admin.style.css' );
	}
	
	
	## Save Data
  public function meta_fields_update( $post_id ){
    if ( !isset($_POST['extra_fields_nonce']) || !wp_verify_nonce($_POST['extra_fields_nonce'], 'nonce') ) return false; // check
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false; // if autosave
    if ( !current_user_can('edit_post', $post_id) ) return false; // if user have rule for edit

    if( !isset($_POST['extra']) && !isset($_POST['custom_field']) ) return false; 

    // custom_field 
    if ( $_POST['custom_field'] ){
      update_post_meta($post_id, 'custom_field', $_POST['custom_field'] );
    } else {
      delete_post_meta($post_id, 'custom_field' );
    }

    
    // Ok!
    $_POST['extra'] = array_map('trim', $_POST['extra']);
    foreach( $_POST['extra'] as $key=>$value ){
      if( empty($value) ){
        delete_post_meta($post_id, $key); // remove if empty
        continue;
      }
      update_post_meta($post_id, $key, $value);
    }
    return $post_id;

  }
	
}