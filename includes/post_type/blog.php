<?php

/*
* NewPostType
*/
 
class Blog {
  
  public $post_type      = "article";
  public $slug_post_type = "article";

  function __construct( ) {
    // register_post_type
    add_action( 'init', array($this,'register_post_type') );
  }

    
  ## register_post_type
  public function register_post_type() {
    $labels = array(
      'name'          => 'Блог', // имя на внутренней
      'singular_name' => 'Добавить запись', // админ панель Добавить->Функцию
      'add_new'       => 'Добавить запись',
      'add_new_item'  => 'Добавить запись', // заголовок тега <title>
      'edit_item'     => 'Изменить запись',
      'new_item'      => 'Новая запись',
      'all_items'     => 'Все записи',
      'view_item'     => 'Посмотреть запись на сайте',
      'search_items'  => 'Найти запись',
      'not_found'     => 'Запись не найдена.',
      'menu_name'     => 'Блог' // имя в админке
    );
    $supports_label = array(
      'title',
      'editor',
      'excerpt', 
      'thumbnail', 
      //'custom-fields'
    );
    $args = array(
      'labels'            => $labels,
      'public'            => true,
      'show_ui'           => true, // показывать интерфейс в админке
      'has_archive'       => false,
      'capability_type'   => 'post',
      'show_in_menu'      => true,
      'show_in_nav_menus' => true,
      'rewrite'           => array('slug' => $this->slug_post_type, 'with_front' => false, 'hierarchical'=> true),
      /* remove in frontend */
      'query_var'           => true,
      'taxonomies'        => array(),
      'menu_icon'         => 'dashicons-welcome-write-blog', // https://developer.wordpress.org/resource/dashicons/
      'menu_position'     => 20, 
      'supports'          => $supports_label
    );
    register_post_type($this->post_type, $args);
  }
    
  
}