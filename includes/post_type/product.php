<?php

/*
* NewPostType
*/
 
class Product {
  
  public $post_type      = "product";
  public $slug_post_type = "product";

  function __construct( ) {
    // register_post_type
    add_action( 'init', array($this,'register_post_type') );
    // create_taxonomy
    add_action( 'init', array($this,'create_taxonomy') );
    // add_meta_boxes
    add_action( 'add_meta_boxes', array($this,'reg_meta_box') );
    // add_post_columns
    add_filter( 'manage_edit-product_columns', array($this,'add_post_columns') ); // manage_edit-{тип поста}_columns
    add_action( 'manage_posts_custom_column', array($this,'fill_post_columns') );
  }

    
  ## register_post_type
  public function register_post_type() {
    $labels = array(
      'name'          => 'Продукция', // имя на внутренней
      'singular_name' => 'Добавить продукт', // админ панель Добавить->Функцию
      'add_new'       => 'Добавить продукт',
      'add_new_item'  => 'Добавить продукт', // заголовок тега <title>
      'edit_item'     => 'Изменить продукт',
      'new_item'      => 'Новый продукт',
      'all_items'     => 'Все продукты',
      'view_item'     => 'Посмотреть продукт на сайте',
      'search_items'  => 'Найти продукт',
      'not_found'     => 'Продукт не найден.',
      'menu_name'     => 'Продукция' // имя в админке
    );
    $supports_label = array(
      'title',
      'comments',
      //'custom-fields'
    );
    $args = array(
      'labels'            => $labels,
      'public'            => true,
      'show_ui'           => true, // показывать интерфейс в админке
      'has_archive'       => false,
      'capability_type'   => 'post',
      'show_in_menu'      => true,
      'show_in_nav_menus' => true,
      'rewrite'           => true,
      'query_var'         => true,
      'taxonomies'        => array('product_cat'),
      'menu_icon'         => 'dashicons-products', // https://developer.wordpress.org/resource/dashicons/
      'menu_position'     => 20, 
      'supports'          => $supports_label
    );
    register_post_type($this->post_type, $args);
  }
    
  ## Регистрируем таксономию
  public function create_taxonomy() {
    
    register_taxonomy(
      'product_cat', // tax name  
      $this->post_type, // post-type name 
      array(
        'label'             => 'Категории',
        'hierarchical'      => true,
        'public'            => true,
        'query_var'         => true,
        'rewrite'           => array('slug' => 'product_cat', 'with_front' => true, 'hierarchical'=> true),
        'show_admin_column' => true, // показывать колонку
        'show_ui'           => true 
      )
    );
    
    register_taxonomy(
      'product_properties', // tax name  
      $this->post_type, // post-type name 
      array(
        'label'             => 'Свойства',
        'hierarchical'      => true,
        'public'            => true,
        'query_var'         => true,
        'rewrite'           => array('slug' => 'naznachenie', 'with_front' => true, 'hierarchical'=> true),
        'show_admin_column' => true, // показывать колонку
        'show_ui'           => true 
      )
    );
  	
    register_taxonomy(
      'ingredients', // tax name  
      $this->post_type, // post-type name 
      array(
        'label'             => 'Ингредиенты',
        'hierarchical'      => true,
        'public'            => true,
        'query_var'         => true,
        'rewrite'           => array('slug' => 'ingredients', 'with_front' => true, 'hierarchical'=> true),
        'show_admin_column' => true, // показывать колонку
        'show_ui'           => true 
      )
    );
    
  }
    
  ## Регистрируем место для meta
  public function reg_meta_box() {
  add_meta_box('', 'Информация о товаре', array($this,'meta_fields_post'), $this->post_type, 'normal', 'high');
  }

  ## Meta
  function meta_fields_post( $post ){
  ?>
	
  <?php 
    // setting for wp_editor()
    $setting = array(
      'wpautop'       => 0, //  wpautop() добавляет параграфы
      'textarea_rows' => 5,
      'media_buttons' => 0,
      'teeny'         => 0,
      'dfw'           => 1,
      'tinymce' => array(
        'toolbar2'         => '',
        'resize'           => false, 
        'wp_autoresize_on' => true
      ),
      'quicktags'        => 1,
      'drag_drop_upload' => false,
    );
   ?>


  <table class="wpstart_admin_table">
    <caption>Миниатюра товара</caption>
    <tr>
      <td style="width: 180px;">
        <?php startwp__add_img('miniature'); ?>
        <p class="description">Миниатюра [500x500]:</p>
      </td>
      <td>
        <?php startwp__add_input("prod_type"); ?>
        <p class="description">Тип продукта:</p>
        <hr><br>
        <?php startwp__add_input_num("prod_price"); ?>
        <p class="description">Цена [грн]:</p>
        <hr><br>
        <?php startwp__add_input_num("prod_price_action"); ?>
        <p class="description">Цена по акции [грн]:</p>
        <hr><br>
        <?php startwp__add_checkbox("prod_popular", "Популярный товар"); ?>
        <hr><br>
        <?php startwp__add_checkbox("prod_action", "Акция"); ?>
      </td>
    </tr>
  </table>

  <table class="wpstart_admin_table">
    <caption>Галерея товара</caption>
    <tr>
      <td>
        <?php startwp__add_gallery('product_gallery'); ?>
      </td>
    </tr>
  </table>

  <table class="wpstart_admin_table">
    <caption>Описание</caption>
    <tr>
      <td>
        <?php
          $prod_descrip = get_post_meta($post->ID,'prod_descrip', true);
          wp_editor( $prod_descrip, 'proddescrip', array_merge($setting, array(
            'textarea_name' => 'extra[prod_descrip]'
            )) 
          );
        ?>
      </td>
    </tr>
  </table>

  <? /*
  <table class="wpstart_admin_table">
    <caption>Состав</caption>
    <tr>
      <td>
        <?php
          $prod_composition = get_post_meta($post->ID,'prod_composition', true);
          wp_editor( $prod_composition, 'prodcomposition', array_merge($setting, array(
            'textarea_name' => 'extra[prod_composition]'
            )) 
          );
        ?>
      </td>
    </tr>
  </table>
  */ ?>


  <table class="wpstart_admin_table">
    <caption>Как пользоваться</caption>
    <tr>
      <td>
        <?php
          $prod_howuse = get_post_meta($post->ID,'prod_howuse', true);
          wp_editor( $prod_howuse, 'prodhowuse', array_merge($setting, array(
            'textarea_name' => 'extra[prod_howuse]'
            )) 
          );
        ?>
      </td>
    </tr>
  </table>

  <table class="wpstart_admin_table">
    <caption>С этим товаром покупают</caption>
    <tr>
      <td>
        <p class="description">Максимум 4 товара</p>
        <select name="custom_field[select_tov][]" class="qqqqqqqqqq" multiple="multiple" />
         
          <?php
            $args_loop = array( 
              'post_type' => 'product',
              'posts_per_page' => -1,
            );
            $custom_loop = new WP_Query( $args_loop ); 
            $sel_v = get_post_meta($post->ID, 'custom_field', 1);

            while ( $custom_loop->have_posts() ) : $custom_loop->the_post(); 

            $the_id = get_the_ID();
            $select = false;

            foreach ($sel_v["select_tov"] as $value) {
              if ( $the_id == $value) $select = true;
            } ?>

            <option value="<?= $the_id; ?>" <?php if ( $select ) echo  'selected="selected"'; ?> ><?php the_title(); ?></option>
            
          <?php 
          endwhile;
          wp_reset_postdata();
          ?> 
        </select>
      </td>
    </tr>
  </table>

  <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce('nonce'); ?>" />
   
  <?php
  }
    

  ## Добавим заголовок колонки
  public function add_post_columns($my_columns){
    $slider = array( 
      'meta_title_prod_post'   => 'Популярный', 
      'meta_title_prod_action' => 'Акция', 
      'meta_title_prod_count'  => 'Просмотры', 
      'meta_title_prod_price'  => 'Цена' 
    );
    $my_columns = array_slice( $my_columns, 0, 4, true ) + $slider + array_slice( $my_columns, 4, NULL, true );
    return $my_columns;
  }
  ## Контент колонки
  public function fill_post_columns( $column ) {
    global $post;
    switch ( $column ) {
      case 'meta_title_prod_post':
        startwp__add_checkbox("prod_popular", "");
        break;
      case 'meta_title_prod_action':
        startwp__add_checkbox("prod_action", "");
        break;
      case 'meta_title_prod_price':
        $price = get_post_meta( $post->ID, 'prod_price', true );
        $prod_price_action = get_post_meta( $post->ID, 'prod_price_action', true );
        echo $price." грн";
        if ( $prod_price_action ) echo "<br><span style='color: #f33; font-size: 14px; font-weight: bold;' >".$prod_price_action." грн</span>";
        break;
      case 'meta_title_prod_count':
        echo getPostViews( $post->ID );
        break;
    }
  }

  
}