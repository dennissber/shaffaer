<?php

####################################################
#### function for custom admin panel (back end) ####
####################################################

## Add input
function startwp__add_input( $field_name ){
  global $post;
  echo '<input type="text" class="w100p" name="extra['.$field_name.']" value="'.get_post_meta($post->ID, $field_name, true).'" />';
}

## Add input number
function startwp__add_input_num( $field_name ){
  global $post;
  echo '<input type="number" class="w100p" name="extra['.$field_name.']" value="'.get_post_meta($post->ID, $field_name, true).'" />';
}

## Add textarea
function startwp__add_textarea( $field_name, $height ){
  global $post;
  echo '<textarea type="text" class="w100p" name="extra['.$field_name.']" style="height: '.$height.'px;">'.get_post_meta($post->ID, $field_name, true).'</textarea>';
}

## Add checkbox ( ajax function "Save_checkbox" below )
function startwp__add_checkbox ($name, $text) {
	global $post;
	$value = get_post_meta($post->ID, $name, true);
	$state = $value == "on" ? checked : "";
	echo '
  <label class="save_my_checkbox__label">
    <input type="checkbox" data-id="'.$post->ID.'" data-key="'.$name.'" class="save_my_checkbox" name="extra['.$name.']" '.$state.' />
    <span></span>
    '.$text.'
  </label>';
}

## Add img
function startwp__add_img($name){
  global $post;
  if ( !$name ){return false;}
  $value   = get_post_meta($post->ID, $name, true);
  $img_url = wp_get_attachment_image_url($value, '100_100' );
  echo
  '
  <div class="my__img__wrap">
    <img src="'.esc_url($img_url).'" alt="NO IMAGES" class="loaded_img" />
    <input type="hidden" class="my_img__src" name="extra['.$name.']" value="'.$value.'" />
    <span class="upload_image_button">Загрузить</span>
    <span class="remove_image_button">&times;</span>
  </div>
  ';
}

## Add gallery
function startwp__add_gallery( $metaName ){
  if ( !$metaName ){return false;}

  global $post;
  $gallery__ids = get_post_meta($post->ID, $metaName, true);
  echo '<span class="add_gallery_block__btn">Добавить изображение</span>';
  echo '<div class="my__gallery__wrap">';
  $gallery_src = explode(" ", $gallery__ids);
  foreach ( $gallery_src as $id ){
    $img_url = wp_get_attachment_image_url($id, '100_100' );
    echo '
    <div class="my__gallery__item"> 
      <img src="'.$img_url.'" class="loaded_img" alt="NO IMAGES" />
      <span class="my__gallery__img_id" hidden>'.$id.'</span>
      <span class="my__gallery__item__add_img">Загрузить</span>
      <span class="my__gallery__item__remove_img">&times;</span>
    </div>
    ';
  }
  echo '<input type="hidden" class="gallery__imgs_id" name="extra['.$metaName.']" " value="'.$gallery__ids.'" />';
  echo '</div>';
}

## Save_checkbox ( for "Add checkbox" above )
add_action('wp_ajax_save_checkbox', 'save_checkbox' );
function save_checkbox ( ){
  $value = $_POST['value'];
  $key   = $_POST['key'];
  $id    = $_POST['id'];
  if ($value === "true"){
    update_post_meta($id, $key, "on");
    exit("ID[$id] / Key[$key] - Checkbox ON.");
  } else {
    delete_post_meta($id, $key);
    exit("ID[$id] / Key[$key] - Checkbox OFF.");
  }
}

## Field_construct v 1.0
function the_field_construct ( $name_field ){
  global $post;
  $custom_field  = get_post_meta($post->ID, $name_field, true);

  if ( !$custom_field  ) return "Пусто.";

  foreach ($custom_field as $key => $value) {
    echo '
      <div class="custom_field_construct__item" data-id="'.$key.'">
        <span class="custom_field_construct__remove_item button button-primary button-large">X</span>
        <table>
          <tr>
            <td>
              <div class="custom_field_construct__item_img_wrap">
                <img src="'.wp_get_attachment_url( $value["image1"] ).'" alt="NO IMAGES" class="custom_field_construct__img" />
                <input type="hidden" name="custom_field['.$key.'][image1]" value="'.$value["image1"].'" class="custom_field_construct__img_input">
              </div>
            </td>
            <td>
              <input type="text" name="custom_field['.$key.'][day]" value="'.$value["day"].'" >
              <input type="text" name="custom_field['.$key.'][data1]" value="'.$value["data1"].'">
              <input type="text" name="custom_field['.$key.'][data2]" value="'.$value["data2"].'">
            </td>
            <td>
              <textarea name="custom_field['.$key.'][text]" >'.$value["text"].'</textarea>
            </td>
          </tr>
        </table>
      </div>';
  }
}


## PostViews
function setPostViews( $postID ){
  $count_key = 'post_views_count';
  $count     = get_post_meta( $postID, $count_key, true );
  if( $count != "" ){
    $count++;
    update_post_meta( $postID, $count_key, $count );
  } else {
    update_post_meta( $postID, $count_key, 0 );
  }
}
function getPostViews( $postID ){
  $count_key = 'post_views_count';
  $count     = get_post_meta( $postID, $count_key, true );
  if( $count != "" ){
    return $count;
  } else {
    update_post_meta( $postID, $count_key, 0 );
    return 0;
  }
}





####################################################
#### function for custom option page  (back end) ###
####################################################

## Add input
function startwp__add_option_input( $field_name ){
  echo '<input type="text" class="w100p" name="'.$field_name.'" value="'.get_option($field_name).'" />';
}

## Add textarea
function startwp__add_option_textarea( $field_name, $height ){
  echo '<textarea type="text" class="w100p" name="'.$field_name.'" style="height: '.$height.'px;">'.get_option( $field_name ).'</textarea>';
}

## Add img
function startwp__add__option_img($name){
  if ( !$name ){return false;}

  $value = get_option($name);
  $img_url = wp_get_attachment_image_url($value, '100_100' );
  echo
  '
  <div class="my__img__wrap">
    <img src="'.esc_url($img_url).'" alt="NO IMAGES" class="loaded_img" />
    <input type="hidden" class="my_img__src" name="'.$name.'" value="'.$value.'" />
    <span class="upload_image_button">Загрузить</span>
    <span class="remove_image_button">&times;</span>
  </div>
  ';
}

## Add gallery
function startwp__add__option_gallery( $metaName ){
  if ( !$metaName ){return false;}

  $gallery__ids = get_option($metaName);
  echo '<span class="add_gallery_block__btn">Добавить изображение</span>';
  echo '<div class="my__gallery__wrap">';
  $gallery_src = explode(" ", $gallery__ids);
  foreach ( $gallery_src as $id ){
    $img_url = wp_get_attachment_image_url($id, '100_100' );
    echo '
    <div class="my__gallery__item"> 
      <img src="'.$img_url.'" class="loaded_img" alt="NO IMAGES" />
      <span class="my__gallery__img_id" hidden>'.$id.'</span>
      <span class="my__gallery__item__add_img">Загрузить</span>
      <span class="my__gallery__item__remove_img">&times;</span>
    </div>
    ';
  }
  echo '<input type="hidden" class="gallery__imgs_id" name="'.$metaName.'" " value="'.$gallery__ids.'" />';
  echo '</div>';
}

## Add file
function startwp__add__option_file($name){
  if ( !$name ){return false;}
  echo
  '
  <div>
    <input type="text" class="w100p" name="'.$name.'" value="'.get_option($name).'" >
    <span class="startwp__add_file__btn">Выбрать файл</span>
  </div>
  ';
}







####################################
#### Other function (front end) ####
####################################

## get_video_id
function get_video_id( $name ){
  preg_match('#(\.be/|/embed/|/v/|/watch\?v=)([A-Za-z0-9_-]{5,11})#', $name, $matches);
  if(isset($matches[2]) && $matches[2] != ''){
    $YoutubeId = $matches[2];
  }
  return $YoutubeId;
}

## give_me filtered content
function give_me__content ( $content ) {
  return apply_filters('the_content', $content);
}

## Shorten Text 
function text_limit($text, $count, $after) {
  $text = mb_substr($text,0,$count);
  return "<p>".$text.$after."</p>";
}

## Price format 999 999 999 
function price_format($text) {
  return strrev(implode(' ', str_split(strrev($text),3))); 
}

## Custom button "More"
add_filter('the_content_more_link', 'my_more_link', 10, 2);
function my_more_link($more_link, $more_link_text) {
  return str_replace($more_link_text, 'Читать полностью', $more_link);
}

## get_gallery
function wps__get_gallery ( $postID, $name ){
 $gallery_ids = get_post_meta($postID, $name, true);
 $gallery     = explode(" ", $gallery_ids);
 return $gallery;
}







#####################################
####### WP admin menu visible #######
#####################################

#### Удаление кнопки "настройки экрана" 
//add_filter('screen_options_show_screen', 'remove_screen_options');
function remove_screen_options(){
  return false;
}

#### Hide top menu item 
add_action( 'wp_before_admin_bar_render', 'wps_admin_bar' );
function wps_admin_bar() {
  global $wp_admin_bar;
  $wp_admin_bar->remove_menu('comments');
  $wp_admin_bar->remove_menu('new-content');
  $wp_admin_bar->remove_menu('about');
  $wp_admin_bar->remove_menu('wporg');
  $wp_admin_bar->remove_menu('documentation');
  $wp_admin_bar->remove_menu('support-forums');
  $wp_admin_bar->remove_menu('feedback');
  $wp_admin_bar->remove_menu('view-site');
}

#### Hide menu item
function remove_menus(){
  //remove_menu_page( 'index.php' );                  //Консоль
  remove_menu_page( 'edit.php' );                   //Записи
  //remove_menu_page( 'upload.php' );                 //Медиафайлы
  //remove_menu_page( 'edit.php?post_type=page' );    //Страницы
  //remove_menu_page( 'edit-comments.php' );          //Комментарии
  //remove_menu_page( 'themes.php' );                 //Внешний вид
  //remove_menu_page( 'plugins.php' );                //Плагины
  //remove_menu_page( 'users.php' );                  //Пользователи
  //remove_menu_page( 'tools.php' );                  //Инструменты
  //remove_menu_page( 'options-general.php' );        //Настройки
}
add_action( 'admin_menu', 'remove_menus' );

#### Admin footer modification
add_filter('admin_footer_text', 'remove_footer_admin');
function remove_footer_admin () {
  echo '<span id="footer-thankyou">Theme by <a href="http://pengstud.com/" target="_blank">Penguin Studio</a></span>';
}

#### save post CTRL + S
add_filter('admin_footer', 'post_save_accesskey');
function post_save_accesskey(){
  if( get_current_screen()->parent_base != 'edit' ) return;
  ?>

  <script type="text/javascript">
  jQuery(document).ready(function($){
    $(window).keydown(function(e){
      // событие ctrl+s - 83 код s
      if( e.ctrlKey && e.keyCode == 83 ){   
        e.preventDefault();
        $submit = $('[name="save"]').click();
      }
    });
  });
  </script>

  <?php
  if ( get_current_screen()->taxonomy == "ingredients" ) {?>

    <script type="text/javascript">
      jQuery(document).ready(function($){
        $(window).keydown(function(e){
          // событие ctrl+s - 83 код s
          if( e.ctrlKey && e.keyCode == 83 ){   
            e.preventDefault();
            $submit = $('.button-primary').click();
          }
        });
      });
    </script>

  <?php
  }
}
function general_admin_notice(){
	global $pagenow;
	if ( $pagenow == 'post.php' ) {
		echo '<div class="notice notice-info is-dismissible">
			<p>Для сохранения записи доступна комбинация "CTRL + S".</p>
		</div>';
	}
}
add_action('admin_notices', 'general_admin_notice');


// удалить верхнюю панель
//add_filter( 'show_admin_bar', '__return_false' );




######################################
#### Other function (development) ####
######################################

// Выводит данные о кол-ве запросов к БД, время выполнения скрипта и размер затраченной памяти.
//add_filter('admin_footer_text', 'performance'); // в подвале админки
//add_filter('wp_footer', 'performance'); // в подвале сайта
function performance( ){
  $stat = sprintf('SQL: %d за %.3f sec. %.2fMB',
    get_num_queries(),
    timer_stop( 0, 3 ),
    memory_get_peak_usage() / 1024 / 1024
  );
  echo $stat;
}
// Какой используется файл шаблона?
//add_action('wp_footer', 'show_template'); // в подвале
function show_template(){
  echo "<br>";
  global $template;
  echo $template;
}
## print_r between <pre></pre>
function pre_print_r( $arg = "" ){
  if ( $arg ) {
    echo '<pre>';
    print_r( $arg );
    echo '</pre>';
  } else {
    echo '!!!';
  }
}

?>