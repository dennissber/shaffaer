<?php

/*
* For Page Settings
*/

class ThemeSettings {
  
  function __construct() {
    add_action( 'admin_menu', array($this,'add_theme_settings_page') );
    add_action( 'admin_init', array($this,'register_mysettings') );
  }
  
  ## добавление раздела меню
  public function add_theme_settings_page(){
		add_menu_page(  
			'Настройки темы',             					// Отображаемый в браузере заголовок для данного меню  
			'Настройки темы',                		    // Текст пункта меню  
			'administrator',              					// Пользователи, которые могут видеть этот пункт меню  
			'wps_theme_settings',                   // Уникальный идентификатор для данного пункта меню  
			array($this,'theme_settings_content')   // Имя функции, вызываемой при отображении меню 
    );  
		/*
		add_submenu_page(  
			'wps_theme_settings',              	    // Регистрация подпункта под определенным выше пунктом меню  
			'Опции Sandbox',            						// Текст заголовка браузера при активированном пункте меню  
			'Опции',                    						// Текст для этого подпункта  
			'administrator',          						  // Группа пользователей, которой доступен этот подпункт  
			'sandbox_options',          						// Уникальный ID - псевдоним – для данного подпункта меню  
			array($this,'theme_settings_content')   // Функция, используемая для вывода содержимого этого пункта меню  
		);
		*/
		
  }
  
  ## поля настроек
  public function register_mysettings() {
    
    // общие
    register_setting( 'settings_group', 'theme_option_contact_email' );
    register_setting( 'settings_group', 'theme_option_contact_phone_1' );
    register_setting( 'settings_group', 'theme_option_contact_phone_2' );

    register_setting( 'settings_group', 'theme_option_contact_schedule_1' );
    register_setting( 'settings_group', 'theme_option_contact_schedule_2' );

    // Контакты
    register_setting( 'settings_group', 'theme_option_contact_page_city' );
    register_setting( 'settings_group', 'theme_option_contact_page_schedule' );
    register_setting( 'settings_group', 'theme_option_contact_page_phone' );
    register_setting( 'settings_group', 'theme_option_contact_page_email' );

    // оплата и доставка
    register_setting( 'settings_group', 'theme_option_item_page_pay_an_delivery' );

    // email baza
    register_setting( 'settings_group', 'theme_option_item_email_baza' );

  }

  public function theme_settings_content() {
  ?>
	
	
  <?php 
    if ( ! isset( $_REQUEST['settings-updated'] ) ) $_REQUEST['settings-updated'] = false;
    if ( false !== $_REQUEST['settings-updated'] ) :
  ?>
    <div id="message" class="updated my-update-message">
      <p><strong>Настройки сохранены</strong></p>
    </div>
  <?php endif;
	?>
	

  <div class="wrap">
  <h2>Настройки темы</h2>

  <form method="post" action="options.php">
    <?php wp_nonce_field ( 'update-options' ); ?>
    <?php settings_fields( 'settings_group' ); ?>
    
    <p class="submit">
      <input type="submit" class="button-primary" value="Сохранить изменения" />
    </p>
    
    <div class="tabs">
      <input id="tab1" type="radio" name="tabs" checked>
      <label for="tab1" title="tab1">Общие</label>
      
      <input id="tab2" type="radio" name="tabs">
      <label for="tab2" title="tab2">Стр. "Контакты"</label>
      
      <input id="tab3" type="radio" name="tabs">
      <label for="tab3" title="tab3">Карточка товара</label>

      <input id="tab4" type="radio" name="tabs">
      <label for="tab4" title="tab4">База e-mail</label>
      
      <!-- Вкладка 1 -->
      <section id="content-tab1">
        <table class="wpstart_admin_table">
          <!--<caption></caption>-->
          <tr>
            <th>E-mail для писем:</th>
            <td>
              <?php startwp__add_option_input('theme_option_contact_email'); ?>
              <p class="description">Можно ввести несколько почтовых адресов разделяя запятой.</p>
            </td>
          </tr>
          <tr>
            <th>Телефон 1:</th>
            <td>
              <?php startwp__add_option_input('theme_option_contact_phone_1'); ?>
            </td>
          </tr>
          <tr>
            <th>Телефон 2:</th>
            <td>
              <?php startwp__add_option_input('theme_option_contact_phone_2'); ?>
            </td>
          </tr>
        </table>
        <table class="wpstart_admin_table">
          <tr>
            <th>График работы:</th>
            <td>
              <?php startwp__add_option_input('theme_option_contact_schedule_1'); ?>
              <p class="description">Дни недели</p>
            </td>
            <td>
              <?php startwp__add_option_input('theme_option_contact_schedule_2'); ?>
              <p class="description">Время</p>
            </td>
          </tr>
        </table>
        
      </section>
      
      <!-- Вкладка 2 -->
      <section id="content-tab2">
        <table class="wpstart_admin_table">
          <tr>
            <th>Город:</th>
            <td>
              <?php startwp__add_option_input('theme_option_contact_page_city'); ?>
            </td>
          </tr>
          <tr>
            <th>График работы:</th>
            <td>
              <?php startwp__add_option_input('theme_option_contact_page_schedule'); ?>
            </td>
          </tr>
          <tr>
            <th>Телефон:</th>
            <td>
              <?php startwp__add_option_input('theme_option_contact_page_phone'); ?>
            </td>
          </tr>
          <tr>
            <th>Публичный e-mail:</th>
            <td>
              <?php startwp__add_option_input('theme_option_contact_page_email'); ?>
            </td>
          </tr>
        </table>
      </section>
      
      <!-- Вкладка 3 -->
      <section id="content-tab3">
        <table class="wpstart_admin_table">
          <tr>
            <th>Оплата и доставка:</th>
            <td>
              <?php startwp__add_option_textarea('theme_option_item_page_pay_an_delivery', 100); ?>
            </td>
          </tr>
        </table>
      </section>
      
      <!-- Вкладка 4 -->
      <section id="content-tab4">
        <table class="wpstart_admin_table">
          <tr>
            <td>
              <?php startwp__add_option_textarea('theme_option_item_email_baza', 150); ?>
            </td>
          </tr>
        </table>
      </section>
    </div>
    
    <p class="submit">
      <input type="submit" class="button-primary" value="Сохранить изменения" />
    </p>

  </form>
  </div>
  <?php 
  }

 
}