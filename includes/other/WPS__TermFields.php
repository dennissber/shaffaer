<?php

/*
* WPS Theme
* Class WPS__TermFields 
* For generate term meta field
* Allow type: textarea, input, image, wp_editor
* Allow rewrite standart meta description
* Need PHP 5.3+
*/


/* example
new WPS__TermFields( 
  array(
    'rewrite_description' => false,     // заменить поле description
    'taxonomy'  => array( 'name_cat' ),
    'fields'    => array(
      array(
        'f_name' => 'name',
        'type'   => 'textarea',
        'title'  => 'Text',
        'desc'   => 'Text.',
      )
    )
  )
);
*/
 
class WPS__TermFields {

  private $options;
  
  function __construct( $option ) {
    $this->options = (object) $option;

    ## rewrite_description
    if ( $this->options->rewrite_description === true ){
      $this->rewrite_description();
    }

    foreach( $this->options->taxonomy as $taxonomy ){
      // when create
      add_action( "{$taxonomy}_add_form_fields",  array( $this, 'add_new_custom_fields'     ) );
      add_action( "create_{$taxonomy}",           array( $this, 'save_custom_taxonomy_meta' ) );
      // when edit
      add_action( "{$taxonomy}_edit_form_fields", array( $this, 'edit_new_custom_fields'    ) );
      add_action( "edited_{$taxonomy}",           array( $this, 'save_custom_taxonomy_meta' ) );
    }
  }



  /* rewrite_description
  * Добавляет поле для мета тега description
  * Выводит description на страницы таксономий
  * Скрывает стандартное поле description в админ-панели
  */
  public function rewrite_description (){

    new WPS__TermFields( 
      array(
        'taxonomy'  => $this->options->taxonomy,
        'fields'    => array(
          array(
            'f_name' => 'seo_description',
            'type'   => 'textarea',
            'title'  => 'Description',
            'desc'   => 'Description для страниц категорий.',
          ),
        )
      )
    );

    add_action( 'wp_head',    array( $this, 'term_description') );
    add_action( 'admin_head', array( $this, 'hidden_term_description') );
  }

  ## term_description
  public function term_description () {
    if ( is_tax() ){
      $cur_cat_obj  = get_queried_object();
      $term_id      = $cur_cat_obj->term_id;
      $seo_descrip  = get_term_meta( $term_id, 'seo_description', true );

      echo "<!-- WPS__Description -->\r\n";
      echo "<meta name='description' content='".$seo_descrip."' />";
    }
  }

  ## hide standart description field // TODO
  // Исравить!!! Сейчас скрывает все поля, а нужно только на страницах выбраных таксономий
  public function hidden_term_description() {
    print '
    <style>
    .term-description-wrap { display:none; }
    </style>
    ';
  } 

  /**
   * Remove the 'description' column from the table in 'edit-tags.php'
   * but only for the 'post_tag' taxonomy
   */
  /*
  add_filter('manage_edit-post_tag_columns', function ( $columns ) 
  {
      if( isset( $columns['description'] ) )
          unset( $columns['description'] );   

      return $columns;
  } );
  */
  



  
  ## edit_new_custom_fields name_cat
  public function edit_new_custom_fields( $term ) {

    $fields = $this->options->fields;

    foreach ($fields as $value) {
      $termID  = $term->term_id;
      $f_name  = $value['f_name'];
      $title   = $value['title'];
      $descrip = $value['desc'];
      $type    = $value['type'];
      $data    = get_term_meta($termID, $f_name, true);

      echo '
      <tr class ="form-field">
        <th scope="row" valign="top">'.$title.'</th>
        <td>';

      switch ( $type ) {
        case 'textarea':
          echo '<textarea type="text" name="wps_term_meta['.$f_name.']" style="height: 80px;">'.$data.'</textarea>';
          echo '<p class="description">'.$descrip.'</p>';
          break;
        case 'input':
          echo '<input type="text" name="wps_term_meta['.$f_name.']" value="'.$data.'" />';
          echo '<p class="description">'.$descrip.'</p>';
          break;
        case 'image':
          $img_url = wp_get_attachment_image_url($data, '100_100' );
          echo '
          <div class="my__img__wrap">
            <img src="'.esc_url($img_url).'" alt="NO IMAGES" class="loaded_img" />
            <input type="hidden" class="my_img__src" name="wps_term_meta['.$f_name.']" value="'.$data.'" />
            <span class="upload_image_button">Загрузить</span>
            <span class="remove_image_button">&times;</span>
          </div>';
          echo '<p class="description">'.$descrip.'</p>';
          break;
        case 'wp_editor':
          wp_editor( $data, $f_name, array(
            'wpautop'       => 0,
            'textarea_rows' => 5,
            'media_buttons' => true,
            'teeny'         => 0,
            'dfw'           => 1,
            'tinymce' => array(
              'resize'           => false, 
              'wp_autoresize_on' => true
            ),
            'quicktags'        => 1,
            'drag_drop_upload' => false,
            'textarea_name' => 'wps_term_meta['.$f_name.']',
          ) );
          echo '<p class="description">'.$descrip.'</p>';
          break;
        default:
          echo "Неверно указан тип поля.";
          break;
      }

      echo '
        </td>
      </tr>';
    }
  }


  ## add_new_custom_fields
  public function add_new_custom_fields( $taxonomy_slug ){

    $fields = $this->options->fields;

    foreach ($fields as $value) {
      $f_name  = $value['f_name'];
      $title   = $value['title'];
      $descrip = $value['desc'];
      $type    = $value['type'];

      echo '<div class="form-field">';

      switch ( $type ) {
        case 'textarea':
          echo '<label for="tag-title">'.$title.'</label>';
          echo '<textarea type="text" name="wps_term_meta['.$f_name.']" style="height: 80px;" ></textarea>';
          break;
        case 'input':
          echo '<label for="tag-title">'.$title.'</label>';
          echo '<input type="text" name="wps_term_meta['.$f_name.']" value="" />';
          break;
        case 'image':
          echo '<label for="tag-title">'.$title.'</label>';
          echo '
          <div class="my__img__wrap">
            <img src="" alt="NO IMAGES" class="loaded_img" />
            <input type="hidden" class="my_img__src" name="wps_term_meta['.$f_name.']" value="" />
            <span class="upload_image_button">Загрузить</span>
            <span class="remove_image_button">&times;</span>
          </div>';
          break;
        case 'wp_editor':
          break;
        default:
          echo "Неверно указан тип поля.";
          break;
      }

      echo '</div>';
    }
  }
  

  ## save_custom_taxonomy_meta
  public function save_custom_taxonomy_meta( $term_id ) {
    if ( ! isset($_POST['wps_term_meta']) ) return;
    if ( ! current_user_can('edit_term', $term_id) ) return;

    $_POST['wps_term_meta'] = array_map('trim', $_POST['wps_term_meta']);
    foreach( $_POST['wps_term_meta'] as $key=>$value ){
      if( empty($value) ){
        delete_term_meta( $term_id, $key );
        continue;
      }
      update_term_meta( $term_id, $key, $value );
    }
    return $term_id;
  }

}


## Get field
function wps__get_term_meta_field( $field_name ){
  $term_id = get_queried_object()->term_id;
  return get_term_meta( $term_id, $field_name, true );
}

## Get description
function wps__get_term_description(){
  $term_id = get_queried_object()->term_id;
  return term_description( $term_id );
}

