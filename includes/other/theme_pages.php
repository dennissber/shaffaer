<?php

/*
* Meta Fields For Page
*/
 
class PageFields {
  
  function __construct( ) {
    // add_meta_boxes
    add_action( 'add_meta_boxes', array($this,'reg_meta_box') );
  }

    
  ## register meta
  public function reg_meta_box() {
    global $post;
    $pageTemplate = get_post_meta($post->ID, '_wp_page_template', true);

    if(!empty($post)){
      if($pageTemplate == 'templates/page_tpl_name.php' ) { // if template "page_tpl_name"
        add_meta_box('', 'More', array($this,'meta_fields_post'), 'page', 'normal', 'high');
      }
    }
		
		## For All Pages
    add_meta_box('', 'More', array($this,'meta_fields_post'), 'page', 'normal', 'high');
      
  }
	
	// setting for wp_editor()
	public $setting = array(
		'wpautop'       => 0, //  wpautop() добавляет параграфы
		'textarea_rows' => 5,
		'media_buttons' => 0,
		'teeny'         => 0,
		'dfw'           => 1,
		'tinymce' => array(
			'toolbar2'         => '',
			'resize'           => false, 
			'wp_autoresize_on' => true
		),
		'quicktags'        => 1,
		'drag_drop_upload' => false,
	);

  ## Meta
  function meta_fields_post( $post ){
  ?>

  <table class="wpstart_admin_table">
    <caption>Текст</caption>
    <tr>
      <td>
        <?php
          $text_1 = get_post_meta($post->ID,'text_1', true);
          wp_editor( $text_1, 'text1', array_merge($this->setting, array(
            'textarea_name' => 'extra[text_1]'
            )) 
          );
        ?>
      </td>
    </tr>
  </table>

  <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce('nonce'); ?>" />
   
  <?php
  }
    

 
}