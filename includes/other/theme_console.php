<?php

/*
* For page "console"
*/

class Console {
  
  function __construct() {
    add_action( 'dashboard_glance_items', array($this, 'add_right_now_info') );
  	add_action('wp_dashboard_setup', array($this, 'add_dashboard_widgets' ) );
  }
  
  
  ## Добавляем все типы записей в виджет "Прямо сейчас" в консоли
  function add_right_now_info( $items ){

    if( ! current_user_can('edit_posts') ) return $items; // выходим
    // типы записей
    $args = array( 'public' => true, '_builtin' => false );
    $post_types = get_post_types( $args, 'object', 'and' );

    foreach( $post_types as $post_type ){
      $num_posts = wp_count_posts( $post_type->name );
      $num       = number_format_i18n( $num_posts->publish );
      $text      = _n( $post_type->labels->singular_name, $post_type->labels->name, intval( $num_posts->publish ) );

      $items[] = "<a href=\"edit.php?post_type=$post_type->name\">$num $text</a>";
    }

    // таксономии
    $taxonomies = get_taxonomies( $args, 'object', 'and' );
    foreach( $taxonomies as $taxonomy ){
      $num_terms = wp_count_terms( $taxonomy->name );
      $num       = number_format_i18n( $num_terms );
      $text      = _n( $taxonomy->labels->singular_name, $taxonomy->labels->name , intval( $num_terms ) );

      $items[] = "<a href='edit-tags.php?taxonomy=$taxonomy->name'>$num $text</a>";
    }

    return $items;
  }
  
  ## 
  public function add_dashboard_widgets() {
    wp_add_dashboard_widget('dashboard_widget', 'Шаблон "Шаффаер"', array($this, 'custom_dashboard_help') );
    //wp_add_dashboard_widget('dashboard_widget2', 'Наш сайт', array($this, 'custom_dashboard_help2') );
  }

  ## 
  function custom_dashboard_help() {
    echo '<p>Добро пожаловать в шаблон WordPress от Penguin-Studio! Нужна помощь? Нужен сайт на WordPress? Свяжитесь с нами. Более подробную информацию Вы сможете найти на нашем сайте: <a href="https://pengstud.com/" target="_blank">Penguin-Studio</a></p><p><img src="https://dp.pengstud.com/wp-content/themes/pengstud/img/logo.svg" alt="Наш сайт"></p>';
  }

  function custom_dashboard_help2() {
    echo '';
  }

   

}