$(document).ready(function(){

	// text
	$('.one_click_form, .contact_page_form, .subscribe_form').submit(function () {
		var msg = $(this);
		var btn = msg.find('input[type=submit]');
		var btn_text = btn.val();
		$.ajax({
			url : theme_ajax.url, 
			type: 'POST', 
			data: msg.serialize(),
			dataType:'json',
			processData: false,
			beforeSend: function(xhr){
				btn.val('Отправка...');
			},
			success: function(data) {
				$('form').trigger('reset');
				if ( data.location ) {
					window.location.replace(data.location+"/");
				} else {
					btn.val(data.success);
					setInterval(function(){ btn.val(btn_text); }, 2500);
				}
			},
			error: function(data){
				if(data.g_error){ btn.val(data.g_error); }
			}
		});
		return false;
	});


});