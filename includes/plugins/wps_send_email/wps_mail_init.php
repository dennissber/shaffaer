<?php

/*
* For Mail Action
*/

class WPS_Mail {

  
  function __construct() {
    // one_click_form
    add_action('wp_ajax_nopriv_one_click_form', array($this, 'one_click_form'));
    add_action('wp_ajax_one_click_form', array($this, 'one_click_form'));

    // contact_page_form
    add_action('wp_ajax_nopriv_contact_page_form', array($this, 'contact_page_form'));
    add_action('wp_ajax_contact_page_form', array($this, 'contact_page_form'));

    // subscribe_form
    add_action('wp_ajax_nopriv_subscribe_form', array($this, 'subscribe_form'));
    add_action('wp_ajax_subscribe_form', array($this, 'subscribe_form'));
		
		// init_script
		add_action( 'wp_enqueue_scripts', array($this,'init_script') );
		
  }
	
  // init_script
  public function init_script (){
    wp_enqueue_script  ( 'mail_action', get_template_directory_uri() .'/includes/plugins/wps_send_email/wps_mail_action.js', array('jquery'), null, true );
    wp_localize_script ( 'mail_action', 'theme_ajax',
      array(
        'url' => admin_url('admin-ajax.php')
      )
    );
  }

	// get_sitename
	public function get_sitename(){
		$site = strtolower( $_SERVER['SERVER_NAME'] );
    if ( substr( $site, 0, 4 ) == 'www.' ) {
      $site = substr( $site, 4 );
    }
    return $site;
	}


  ## one_click_form
  public function one_click_form(){

    $to           = get_option('theme_option_contact_email');
    $sender       = 'wordpress@' . $this->get_sitename();
    $project_name = get_option('blogname')." ";
    $subject      = "Заказ одним кликом";
    //$sender       = "wordpress@test.pengstud.com";
    //$project_name = "NameSite.com ";

    /* fields */
    $user_name   = $_POST["user_name"];
    $user_phone  = $_POST["user_phone"];
    $item_id     = $_POST["item_id"];

    // get post
    if ( $item_id ) {
    	$the_post   = get_post( $item_id );
    	$item_title = $the_post->post_title;
      $item_link  = get_permalink( $item_id );
    } else {
    	die();
    }

    /* msg */
    $message  = '<html><body>';
    //$message .= '<table border="1" bgcolor="#F8F8F8" width="100%">';
		$message .= '<table cellspacing="0" align="center" border="1" bgcolor="#F8F8F8" cellpadding="0" style="width:100%; max-width:600px;" >';
    $message .= '
          <tr><td colspan="2" style="padding: 10px 10px; text-align:center;">Купить в один клик:</td></tr>
          <tr><td colspan="2" style="padding: 5px 10px; text-align:center;">Данные отправителя:</td></tr>
          <tr>
            <td width="30%" style="padding: 5px 10px;">Имя:</td>
            <td style="padding: 4px 8px;">'.$user_name.'</td>
          </tr>
          <tr>
            <td width="30%" style="padding: 5px 10px;">Телефон:</td>
            <td style="padding: 4px 8px;">'.$user_phone.'</td>
          </tr>
          <tr><td colspan="2" style="padding: 5px 10px; text-align:center;">Данные заказа:</td></tr>
          <tr>
            <td width="30%" style="padding: 5px 10px;">Название товара:</td>
            <td style="padding: 4px 8px;">'.$item_title.'</td>
          </tr>
          <tr>
            <td width="30%" style="padding: 5px 10px;">Ссылка на товар:</td>
            <td style="padding: 4px 8px;">'.$item_link.'</td>
          </tr>';
    $message .= '</table>';
    $message .= '</body></html>';

    /* header */
    $headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
    $headers .= 'From: ' . $project_name . '<'. $sender . '>';

    /* send mail */
    if( wp_mail( $to, $subject, $message, $headers ) ) {
      $result['success'] = "Письмо отправлено!";
      $result['location'] = "spasibo-za-zakaz";
    } else {
      $result['g_error'] = "Произошла ошибка!";
    }
    exit( json_encode($result) );
  }


  ## contact_page_form
  public function contact_page_form(){

    $to           = get_option('theme_option_contact_email');
    $sender       = 'wordpress@' . $this->get_sitename();
    $project_name = get_option('blogname')." ";
    $subject      = "Вопрос";
    //$sender       = "wordpress@test.pengstud.com";
    //$project_name = "NameSite.com ";

    /* fields */
    $user_name    = $_POST["user_name"];
    $user_phone   = $_POST["user_phone"];
    $user_message = $_POST["user_message"];

    /* msg */
    $message  = '<html><body>';
    //$message .= '<table border="1" bgcolor="#F8F8F8" width="100%">';
		$message .= '<table cellspacing="0" align="center" border="1" bgcolor="#F8F8F8" cellpadding="0" style="width:100%; max-width:600px;" >';
    $message .= '
          <tr><td colspan="2" style="padding: 5px 10px; text-align:center;">Данные отправителя:</td></tr>
          <tr>
            <td width="30%" style="padding: 5px 10px;">Имя:</td>
            <td style="padding: 4px 8px;">'.$user_name.'</td>
          </tr>
          <tr>
            <td width="30%" style="padding: 5px 10px;">Телефон:</td>
            <td style="padding: 4px 8px;">'.$user_phone.'</td>
          </tr>
          <tr>
            <td width="30%" style="padding: 5px 10px;">Сообщение:</td>
            <td style="padding: 4px 8px;">'.$user_message.'</td>
          </tr>';
    $message .= '</table>';
    $message .= '</body></html>';

    /* header */
    $headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
    $headers .= 'From: ' . $project_name . '<'. $sender . '>';

    /* send mail */
    if( wp_mail( $to, $subject, $message, $headers ) ) {
      $result['success'] = "Письмо отправлено!";
      //$result['location'] = "spasibo-za-zakaz";
    } else {
      $result['g_error'] = "Произошла ошибка!";
    }
    exit( json_encode($result) );
  }


  ## subscribe_form
  public function subscribe_form(){

    $to           = get_option('theme_option_contact_email');
    $sender       = 'wordpress@' . $this->get_sitename();
    $project_name = get_option('blogname')." ";
    $subject      = "Подписаться";
    //$sender       = "wordpress@test.pengstud.com";
    //$project_name = "NameSite.com ";

    /* fields */
    $user_email = $_POST["user_email"];

    /* write in baza email */
    $cur_text = get_option('theme_option_item_email_baza');
    $cur_text .= $user_email."\r\n";
    update_option( 'theme_option_item_email_baza', $cur_text );
    
    /* msg */
    $message  = '<html><body>';
    $message .= '<table border="1" bgcolor="#F8F8F8" width="100%">';
    $message .= '
        <tr><td colspan="2" style="padding: 5px 10px; text-align:center;">Subscribe:</td></tr>
        <tr>
          <td width="30%" style="padding: 5px 10px;">E-mail:</td>
          <td style="padding: 4px 8px;">'.$user_email.'</td>
        </tr>';
    $message .= '</table>';
    $message .= '</body></html>';

    /* header */
    $headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
    $headers .= 'From: ' . $project_name . '<'. $sender . '>';

    /* send mail */
    if( wp_mail( $to, $subject, $message, $headers ) ) {
      $result['success'] = "Подписка оформлена!";
      exit( json_encode($result) );
    } else {
      $result['g_error'] = "Произошла ошибка!";
      exit( json_encode($result) );
    }

  }



}