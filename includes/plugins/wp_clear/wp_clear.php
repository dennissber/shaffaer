<?php

###########################
####### ClearWp-head ######
###########################

// удаление заголовков, связанных с REST API start
remove_action( 'wp_head', 'rest_output_link_wp_head', 10);
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10);
remove_action( 'wp_head', 'wp_oembed_add_host_js');
remove_action( 'template_redirect', 'rest_output_link_header', 11, 0);
// лишние meta-заголовки
remove_filter('comment_text', 'make_clickable', 9);
remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );
remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
// удаление версии WordPress start 
add_filter('the_generator', 'remove_wpversion');
function remove_wpversion() {
	return '';
}
// удаление версии WordPress из ссылок на скрипты start
add_filter( 'style_loader_src', 'wp_version_js_css', 9999);
add_filter( 'script_loader_src', 'wp_version_js_css', 9999);
function wp_version_js_css($src) {
	if (strpos($src, 'ver=' . get_bloginfo('version')))
		$src = remove_query_arg('ver', $src);
	return $src;
}
add_filter('emoji_svg_url', '__return_empty_string');
// полное отключение Emoji start
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );
// удалить WP_Widget_Recent_Comments css
function remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
}
add_action('widgets_init', 'remove_recent_comments_style');

#### Remove url
add_filter('wp_get_attachment_image_attributes', 'attachment_image');
function attachment_image($attrs) {
	$url_sectors = preg_split('/\//', $attrs['src']);
	unset($url_sectors[0]);
	unset($url_sectors[1]);
	unset($url_sectors[2]);
	$attrs['src'] = '/'.join('/', $url_sectors);
	return $attrs;
}
