(function() {
    tinymce.create('tinymce.plugins.Wptuts', {
        init : function(ed, url) {
            ed.addButton('bold', {
                title : 'Выделить текст',
                cmd   : 'bold',
								//text: ""
            });
            ed.addCommand('bold', function() {
                var selected_text = ed.selection.getContent();
                var return_text = '';
                return_text = '<span class="strong">' + selected_text + '</span>';
                ed.execCommand('mceInsertContent', 0, return_text);
            });
        },
    });
    // Register plugin
    tinymce.PluginManager.add( 'wptuts', tinymce.plugins.Wptuts );
})();
