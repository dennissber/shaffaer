$(document).ready(function(){

	/* cart add to cart */
	$('.item_buy_btn').click(function () {
		var good_id = $(this).attr("data-id");

		var data = {
			action: "cart_action",
			id : good_id
		};
		
		$.ajax({
			url: theme_ajax.url,
			type: "POST",
			data: data,
			success: function(data) {
				//console.log(data);
				$(".cart_counter_text").text(data);
				//modalOpen("order_modal");
				//cartnotify();
			},
			error: function(data){
			}
		});
		return false;
	});

	/* cart remove item */
	$('.cart_item__remove').click(function () {
		var good_uniId = $(this).attr("data-uniId");

		var data = {
			action: "remove_cart_item",
			good_uniId : good_uniId
		};

		$.ajax({
			url: theme_ajax.url,
			type: "POST",
			data: data,
			success: function(data) {
				location.reload();
			},
			error: function(data){
				console.error("remove error :(");
			}
		});
		return false;
	});

	/* cart update */
	$(".cart__update_btn").click( function(){
  	var goods = [];

  	$(".quantity").each(function(){
  		var good_uniId = $(this).attr("data-uniId");
  		var item_count = $(this).text();
  		goods.push( [ good_uniId, item_count ] );
  	});

  	var data = {
			action: "update_cart_items",
			goods: goods
		};

		$.ajax({
			url: theme_ajax.url,
			type: "POST",
			data: data,
			success: function(data) {
				//location.reload();
				window.location.replace("checkout/");
			},
			error: function(data){
				console.error("update error :(");
			}
		});
		return false;
  });

  $(".cart_counter_item_js").change( function(){
  	$(".cart_next__btn").prop('disabled', true);
  });
 

  // cart_order_form
	$('.cart_order_form').submit(function () {
		var msg = $(this);
		var btn = msg.find('input[type=submit]');
		var btn_text = btn.val();

		$.ajax({
			url : theme_ajax.url, 
			type: 'POST', 
			data: msg.serialize(),
			dataType:'json',
			processData: false,
			beforeSend: function(xhr){
				btn.val('Отправка...');
			},
			success: function(data) {
				$('form').trigger('reset');
				location.reload();
				window.location.replace("thanks_page/");
			},
			error: function(data){
				btn.val('Ошибка');
			}
		});
		return false;
	});

});

