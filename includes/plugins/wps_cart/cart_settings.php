<?php

/*
* For Page Settings
*/

class CartSettings {
  
  function __construct() {
    add_action( 'admin_menu', array($this,'add_theme_settings_page') );
    add_action( 'admin_init', array($this,'register_mysettings') );
  }
  
  ## добавление раздела меню
  public function add_theme_settings_page(){
		add_submenu_page(  
			'wps_theme_settings',              	    // Регистрация подпункта под определенным выше пунктом меню  
			'Настройки магазина',            						// Текст заголовка браузера при активированном пункте меню  
			'Настройки магазина',                    						// Текст для этого подпункта  
			'administrator',          						  // Группа пользователей, которой доступен этот подпункт  
			'wps_cart_options',          						// Уникальный ID - псевдоним – для данного подпункта меню  
			array($this,'theme_settings_content')   // Функция, используемая для вывода содержимого этого пункта меню  
		);
  }
  
  ## поля настроек
  public function register_mysettings() {
    // общие
    register_setting( 'wps_cart_settings_group', 'cart_option_contact_email' );
  }

  public function theme_settings_content() {
  ?>
  <?php 
    if ( ! isset( $_REQUEST['settings-updated'] ) ) $_REQUEST['settings-updated'] = false;
    if ( false !== $_REQUEST['settings-updated'] ) :
  ?>
    <div id="message" class="updated my-update-message">
      <p><strong>Настройки сохранены</strong></p>
    </div>
  <?php endif; ?>

  <div class="wrap">
  <h2>Настройки корзины</h2>

  <form method="post" action="options.php">
    <?php wp_nonce_field ( 'update-options' ); ?>
    <?php settings_fields( 'wps_cart_settings_group' ); ?>
    
    <p class="submit">
      <input type="submit" class="button-primary" value="Сохранить изменения" />
    </p>
    
    <div class="tabs">
      <input id="tab1" type="radio" name="tabs" checked>
      <label for="tab1" title="tab1">Общие</label>
      
      <!--
      <input id="tab2" type="radio" name="tabs">
      <label for="tab2" title="tab2">Вкладка 2</label>
      <input id="tab3" type="radio" name="tabs">
      <label for="tab3" title="tab3">Вкладка 3</label>
      <input id="tab4" type="radio" name="tabs">
      <label for="tab4" title="tab4">Вкладка 4</label>
      -->
      
      <!-- Вкладка 1 -->
      <section id="content-tab1">
        <table class="wpstart_admin_table">
          <tr>
            <th>E-mail для писем:</th>
            <td>
              <?php startwp__add_option_input('cart_option_contact_email'); ?>
              <p class="description">Можно ввести несколько почтовых адресов разделяя запятой.</p>
            </td>
          </tr>
        </table>
        
      </section>
      
      <!-- Вкладка 2 -->
      <section id="content-tab2">
      2
      </section>
      
      <!-- Вкладка 3 -->
      <section id="content-tab3">
      3
      </section>
      
      <!-- Вкладка 4 -->
      <section id="content-tab4">
      4
      </section>
    </div>
    
    <p class="submit">
      <input type="submit" class="button-primary" value="Сохранить изменения" />
    </p>

  </form>
  </div>
  <?php 
  }

 
}