<?php

class WPS_Cart {

	public $domain_name;
	
	function __construct( ) {

		$this->domain_name = $this->wps__get_sitename();

		// addToCart
		add_action( 'wp_ajax_nopriv_cart_action', array($this,'addToCart') );
		add_action( 'wp_ajax_cart_action', array($this,'addToCart') );

		// removeFromCart
		add_action( 'wp_ajax_nopriv_remove_cart_item', array($this,'removeFromCart') );
		add_action( 'wp_ajax_remove_cart_item', array($this,'removeFromCart') );

		// updateCart
		add_action( 'wp_ajax_nopriv_update_cart_items', array($this,'updateCart') );
		add_action( 'wp_ajax_update_cart_items', array($this,'updateCart') );

		// Mail
		add_action( 'wp_ajax_nopriv_cart_order_form', array($this,'order_mail') );
		add_action( 'wp_ajax_cart_order_form', array($this,'order_mail') );

		// init_script
		add_action( 'wp_enqueue_scripts', array($this,'init_script') );

		// init Order list
		require_once 'orders.php';
		new Orders();
		// init Order list
		require_once 'cart_settings.php';
		new CartSettings();
	}
	
	// init_script
	public function init_script (){
		wp_enqueue_script  ( 'cart_script', get_template_directory_uri() .'/includes/plugins/wps_cart/cart_script.js', array('jquery'), null, true );
		/*
		wp_localize_script ( 'cart_script', 'theme_ajax',
			array(
				'url' => admin_url('admin-ajax.php')
			)
	  );
	  */
	}

	/* wps__get_sitename */
	public function wps__get_sitename(){
	  $site_name = strtolower( $_SERVER['SERVER_NAME'] );
	  if ( substr( $site_name, 0, 4 ) == 'www.' ) {
	    $site_name = substr( $site_name, 4 );
	  }
	  return $site_name;
	}


	/* addToCart */
	public function addToCart(){
		$post_id      = htmlspecialchars($_POST['id']);

		$prod_price   = get_post_meta( $post_id, "prod_price", true);
		$prod_price_action = get_post_meta( $post_id, "prod_price_action", true );
		if ( $prod_price_action ) {
			$prod_price = $prod_price_action;
		}
		$goods_obj = getCartCookie();
		$uniq_Id   = uniq_hash(1);

		foreach($goods_obj as $current_item){
			if ( $post_id == $current_item->id ){
				$current_item->count++;
				$this->cartUpdateCookie($goods_obj);
				echo cartPriceCount()+$prod_price;
				die();
			} 
		}

		$goods_obj->$uniq_Id = array( 
			"uniqId"  => $uniq_Id,
			"id"      => $post_id,
			"count"   => 1,
			"price"   => $prod_price
		);

		//pre_print_r( $goods_obj );

		$this->cartUpdateCookie($goods_obj);
		echo cartPriceCount()+$prod_price;
		die();
	}


	/* removeFromCart */
	public function removeFromCart(){
		$good_uniId = htmlspecialchars($_POST['good_uniId']);
		$goods_obj  = getCartCookie();

		if ( $goods_obj != "" && $good_uniId != "" ) {
			unset($goods_obj->$good_uniId);
			$this->cartUpdateCookie($goods_obj);
		}
		die();
	}


	/* Update Cart */
	public function updateCart(){
		$goods      = $_POST['goods'];
		$goods_obj  = getCartCookie();
		foreach($goods as $val){
			$good_uniId = $val[0];
			$post_count = $val[1];
			$current_item = $goods_obj->$good_uniId;
			$current_item->count = $post_count;
		}
		$this->cartUpdateCookie($goods_obj);
		
		die();
	}

	public function cartUpdateCookie($data){
		setcookie("cart_item", json_encode($data), time()+3600*5, '/', $this->domain_name );
		//setcookie("cart_item", json_encode($data), false, '/', false);
	}
	public function cartRemoveCookie(){
		setcookie("cart_item", json_encode(), time()+3600*5, '/', $this->domain_name );
		//setcookie("cart_item", json_encode(), false, '/', false);
	}

	public function cartCallProduct(){
		if( $_COOKIE['cart_item'] ){
			$goods_obj = getCartCookie();

			foreach($goods_obj as $value) {
				$query = get_post($value->id);
				$query->uniqId  = $value->uniqId;
				$query->count   = $value->count;
				$query->price   = $value->price;
				$product[] = $query;
			}
		}
		return $product;
	}

	// get_sitename
  public function get_sitename(){
    $site = strtolower( $_SERVER['SERVER_NAME'] );
    if ( substr( $site, 0, 4 ) == 'www.' ) {
      $site = substr( $site, 4 );
    }
    return $site;
  }



	#### https://wp-kama.ru/function/wp_insert_post
	public function order_mail(){

		/* user data */
		$user_name     = $_POST['user_name'];
		$user_subname  = $_POST['user_subname'];
		$user_phone    = $_POST['user_phone'];
		$shipping      = $_POST['shipping'];
		$user_area     = $_POST['user_area'];
		$user_city     = $_POST['user_city'];
		$user_street   = $_POST['user_street'];
		/* end user data */

	  $order_id = uniq_hash(4);

		$goods_obj = getCartCookie();

		/* order template in admin */
		#### ORDER 
		$order_items .= '
		<table cellspacing="0" align="center" border="1" bgcolor="#F8F8F8" cellpadding="0" style="width:100%; table-layout: fixed;" >
	    <tr>
	      <td style="padding: 10px;">Товар</td>
	      <td style="padding: 10px;">Цена</td>
	      <td style="padding: 10px;">Количество</td>
	      <td style="padding: 10px;">Итого</td>
	    </tr>';

	  $summ = 0;

		foreach($goods_obj as $value) {
			$title     = get_the_title( $value->id );
			$count     = $value->count;
			$price     = $value->price;
			$common_p  = $count * $price;

			$summ += $common_p;

			$order_items .= '
			<tr>
        <td style="padding: 10px;">«'.$title.'»</td>
        <td style="padding: 10px;">'.$price.' грн</td>
        <td style="padding: 10px;">'.$count.' шт</td>
        <td style="padding: 10px;">'.$common_p.' грн</td>
      </tr>';
		}
		$order_items .= '
		<tr>
      <td style="padding: 10px;" colspan="3" >Всего к оплате</td>
      <td style="padding: 10px;">'.$summ.' грн</td>
    </tr>
    </table>';

    #### USER 
    $user_info .= '
		<table cellspacing="0" align="center" border="1" bgcolor="#F8F8F8" cellpadding="0" style="width:100%; table-layout: fixed;" >
	    <tr>
	      <td style="padding: 10px;">ФИО</td>
	      <td style="padding: 10px;">'.$user_name.' '.$user_subname.'</td>
	    </tr>
	    <tr>
	      <td style="padding: 10px;">Телефон</td>
	      <td style="padding: 10px;">'.$user_phone.'</td>
	    </tr>
	    <tr>
	      <td style="padding: 10px;">Доставка</td>
	      <td style="padding: 10px;">'.$shipping.'</td>
	    </tr>
	    <tr>
	      <td style="padding: 10px;">Область</td>
	      <td style="padding: 10px;">'.$user_area.'</td>
	    </tr>
	    <tr>
	      <td style="padding: 10px;">Город</td>
	      <td style="padding: 10px;">'.$user_city.'</td>
	    </tr>
	    <tr>
	      <td style="padding: 10px;">Отделение НП</td>
	      <td style="padding: 10px;">'.$user_street.'</td>
	    </tr>
	  </table>';

		$order_detail = array(
		  'post_title'   => $order_id,
			'post_type'    => 'orders',
		  'post_status'  => 'publish',
		  'meta_input'   => array(
		  	'description_order' => $order_items, 
		  	'user_info'         => $user_info, 
		  	'order_price'       => $summ, 
		  	)
		);

		$post_id = wp_insert_post( $order_detail );
		/* end order template in admin */

		#### MAIL

    /* base */
    $to           = get_option('theme_option_contact_email');
    $sender       = 'wordpress@' . $this->get_sitename();
    $project_name = get_option('blogname')." ";
    $subject      = "Новый заказ";
    //$sender       = "wordpress@test.pengstud.com";
    //$project_name = "NameSite.com ";

		/* msg */
    $message  = '<html><body>';
    $message .= '
    <table cellspacing="0" align="center" border="1" bgcolor="#F8F8F8" cellpadding="0" style="width:100%; border-bottom:none; table-layout: fixed;" ><tr><td colspan="2" style="padding: 10px 10px; text-align:center;">Заказ №'.$order_id.'</td></tr>
   		<tr><td colspan="2" style="padding: 5px 10px; text-align:center;">Состав заказа:</td></tr>
    </table>';
    $message .= $order_items;
    $message .= '
    <br>
    <table cellspacing="0" align="center" border="1" bgcolor="#F8F8F8" cellpadding="0" style="width:100%; border-bottom:none; table-layout: fixed;" >
    	<tr><td colspan="2" style="padding: 5px 10px; text-align:center;">Данные отправителя:</td></tr>
    </table>';
    $message .= $user_info;
    $message .= '</body></html>';

    /* header */
    $headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
    $headers .= 'From: ' . $project_name . '<'. $sender . '>';

    // send mail
    wp_mail( $to, $subject, $message, $headers );

		// remove items
		$this->cartRemoveCookie();

		exit( json_encode("Успешно") );
	}

} // end Class

function getCartCookie (){
	return json_decode(html_entity_decode(stripslashes($_COOKIE["cart_item"]), ENT_QUOTES,'UTF-8'));
}

function cartCallCount(){ 
	if( $_COOKIE['cart_item'] ){
		$countCall = 0;
		$goods_obj = getCartCookie();
		if ( $goods_obj != "" ) {
			foreach($goods_obj as $value) {
				$countCall += $value->count;
			}
		}
	}
	return $countCall;
}


function cartPriceCount(){ 
	if( $_COOKIE['cart_item'] ){
		$goods_obj = getCartCookie();
		if ( $goods_obj != "" ) {
			foreach($goods_obj as $value) {
				$countPrice += $value->price * $value->count;
			}
		}
	}
	if ( $countPrice == 0 ) $countPrice = 0;
	return $countPrice;
}

function uniq_hash($n=4){
  $chars   = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $randabc = '';
  for($ichars = 0; $ichars < $n; ++$ichars) {
    $random   = str_shuffle($chars);
    $randabc .= $random[0];
  }
  $time=time();
  return substr_replace($time,$randabc,3,0); 
}  
 

?>