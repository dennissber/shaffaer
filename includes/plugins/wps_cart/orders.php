<?php

/*
* NewPostType
*/
 
class Orders {
  
  public $post_type      = "orders";
  public $slug_post_type = "orders";

  function __construct( ) {
    add_action( 'init', array($this,'register_post_type') );
    add_action( 'add_meta_boxes', array($this,'reg_meta_box') );

    // add_post_columns
    add_filter( 'manage_edit-orders_columns', array($this,'add_post_columns') ); // manage_edit-{тип поста}_columns
    add_action( 'manage_posts_custom_column', array($this,'fill_post_columns') );
  }

    
  ## register_post_type
  public function register_post_type() {
    $labels = array(
      'name'          => 'Заказы', // имя на внутренней
      'singular_name' => 'Изменить Заказ', // админ панель Добавить->Функцию
      'add_new'       => 'Новый Заказ',
      'add_new_item'  => 'Новый Заказ', // заголовок тега <title>
      'edit_item'     => 'Изменить Заказ',
      'new_item'      => 'Новый Заказ',
      'all_items'     => 'Все заказы',
      'view_item'     => 'Посмотреть заказ на сайте',
      'search_items'  => 'Найти заказ',
      'not_found'     => 'Заказ не найден.',
      'menu_name'     => 'Заказы' // имя в админке
    );
    $supports_label = array(
      'title'
    );
    $args = array(
      'labels'              => $labels,
      'public'              => false,
      'show_ui'             => true, // показывать интерфейс в админке
      'has_archive'         => false,
      'capability_type'     => 'post',
      'show_in_menu'        => true,
      'show_in_nav_menus'   => true,
      /* remove in frontend */
      //'query_var'           => false, 
      //'publicly_queryable'  => false,
      //'exclude_from_search' => false,
      /* end remove in frontend */
      'menu_icon'           => 'dashicons-cart', // https://developer.wordpress.org/resource/dashicons/
      'menu_position'       => 20, 
      'supports'            => $supports_label
    );
    register_post_type($this->post_type, $args);
  }
    
    
  ## Регистрируем место для meta
  public function reg_meta_box() {
  add_meta_box('', 'Заказ', array($this,'meta_fields_post'), $this->post_type, 'normal', 'high');
  }

  ## Meta
  function meta_fields_post( $post ){
  ?>

  <table class="wpstart_admin_table" style="border: none;">
    <caption>Состав заказа</caption>
    <tr style="border: none;">
      <td style="padding: 0px;">
        <?php echo get_post_meta($post->ID, "description_order", true); ?>
      </td>
    </tr>
  </table>

  <table class="wpstart_admin_table" style="border: none;">
    <caption>Данные пользователя</caption>
    <tr style="border: none;">
      <td style="padding: 0px;">
        <?php echo get_post_meta($post->ID, "user_info", true); ?>
      </td>
    </tr>
  </table>
   
  <?php
  }

  ## Добавим заголовок колонки
  public function add_post_columns($my_columns){
    $slider     = array( 'meta_title_prod_ready' => 'Выполнен', 'meta_title_order_price' => 'Сумма' );
    $my_columns = array_slice( $my_columns, 0, 2, true ) + $slider + array_slice( $my_columns, 2, NULL, true );
    return $my_columns;
  }

  ## Контент колонки
  public function fill_post_columns( $column ) {
    global $post;
    switch ( $column ) {
      case 'meta_title_prod_ready':
        startwp__add_checkbox("prod_ready", "");
        break;
      case 'meta_title_order_price':
        $summ = get_post_meta( $post->ID, 'order_price', true );
        echo $summ.' грн';
        break;
    }
  }
    

}