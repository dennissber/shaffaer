/* like__btn script */
$(".like__btn").click(function(){

	var btn = $(this);
	var id = btn.attr("data-id");

	if ( !id || id == "" ){
		console.warn("Нет ID;")
		return false;
	}

	var data = {
		action: 'post__like', 
		id: id
	};
	$.ajax({
		url: theme_ajax.url,
		type: "POST",
		data: data,
		beforeSend: function() {
			btn.prop('disabled', true);
		},
		success: function(data) {
			if ( btn.hasClass("active") ){
				btn.removeClass("active");
			} else {
				btn.addClass("active");
			}
			btn.text(data);
			btn.prop('disabled', false);
		},
	});
	return false;

});