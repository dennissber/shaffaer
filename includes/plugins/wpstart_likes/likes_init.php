<?php

/*
* Likes for WPS
*/

class WPstartLikes {
	
	function __construct( ) {
		add_action( 'wp_ajax_nopriv_post__like', array($this,'post__like') );
		add_action( 'wp_ajax_post__like', array($this,'post__like') );
		
		// init_script
		add_action( 'wp_enqueue_scripts', array($this,'init_script') );
	}
	
	// init_script
	public function init_script (){
		wp_enqueue_script  ( 'wps_likes_script', get_template_directory_uri() .'/includes/plugins/wpstart_likes/wps_likes.js', array('jquery'), null, true );
		wp_localize_script ( 'wps_likes_script', 'theme_ajax',
			array(
				'url' => admin_url('admin-ajax.php')
			)
    );
	}
	
	public function the_minus($current){
		return (string) --$current;
	}
	public function the_plus($current){
		return (string) ++$current;
	}
	
	function post__like (){
		$id = htmlspecialchars($_POST['id']);

		$likes   = json_decode(html_entity_decode(stripslashes($_COOKIE["like_count"]), ENT_QUOTES,'UTF-8'));
		$current = get_post_meta($id, "the_post_likes", 1);

		if ( $likes->$id == "1" ) {
			$new = $this->the_minus( $current );
			$likes->$id="0";
		} else {
			$new = $this->the_plus( $current );
			$likes->$id="1";
		}

		setcookie("like_count", json_encode($likes), 3*time(), '/', 'domain');

		$new = ($new <= 0 ? "0" : $new);
		update_post_meta($id, "the_post_likes", $new);
		exit( $new );
	}
	
	function setActiveLikes($id){
		$likes = json_decode(html_entity_decode(stripslashes($_COOKIE["like_count"]), ENT_QUOTES,'UTF-8'));
		if ( $likes == "" ) return false;
		foreach ($likes as $key => $value) {
			if ( $key == $id && $value == "1") return "active";
		}
	}
	
	/* 
	<button class="block__event-card__like-btn like__btn fa fa-heart <?php echo $WPLikes->setActiveLikes($post->ID); ?>" data-id="<?php echo $post->ID; ?>" aria-hidden="true"><?php echo get_post_meta($post->ID, "the_post_likes", 1); ?></button>
	*/
	
}
