jQuery(function () {
	$ = jQuery;   

	/* MyCustomImages */
	$('.upload_image_button').live('click',function () {
		var send_attachment_bkp = wp.media.editor.send.attachment;
		var button = $(this);
		wp.media.editor.send.attachment = function (props, attachment) {
			button.siblings(".loaded_img").attr('src', attachment.url);
			button.siblings(".my_img__src").val(attachment.id);
			wp.media.editor.send.attachment = send_attachment_bkp;
		};
		wp.media.editor.open(button);
		return false;
	});
	$('.remove_image_button').live('click',function () {
		var button = $(this);
		if(confirm('Удалить фото?')) {
			button.siblings(".loaded_img").attr('src', '');
			button.siblings(".my_img__src").val('');
			return false;
		}
	});
	/* end MyCustomImages */


	/* MyCustomGallery */
	$('.my__gallery__item__add_img').live('click',function () {
		var send_attachment_bkp = wp.media.editor.send.attachment;
		var button = $(this);
		var this_gallery__wrap = $(this).parent().parent();
		wp.media.editor.send.attachment = function (props, attachment) {
			button.siblings(".loaded_img").attr('src', attachment.url);
			button.siblings(".my__gallery__img_id").text(attachment.id);
			wp.media.editor.send.attachment = send_attachment_bkp;
			search_id(this_gallery__wrap);
		};
		wp.media.editor.open(button);
		return false;
	});

	$('.my__gallery__item__remove_img').live('click',function () {
		var this_gallery__wrap = $(this).parent().parent();
		if(confirm('Удалить фото?')) {
			$(this).parent().remove();
			search_id( this_gallery__wrap );
			return false;
		}
	});
	function search_id (this_gallery__wrap){
	var text = "";
	this_gallery__wrap.find(".my__gallery__img_id").each( function () {
		if ( $(this).text() != "") {
			text += $(this).text() + " ";
		}
	});
	this_gallery__wrap.find(".gallery__imgs_id").val( text );
	}
	$('.add_gallery_block__btn').live('click',function () {
		var gallery_item =
			'<div class="my__gallery__item">' +
				'<img src="" class="loaded_img" alt="NO IMAGES" />' +
				'<span class="my__gallery__img_id" hidden></span>' +
				'<span class="my__gallery__item__add_img">Загрузить</span>' +
				'<span class="my__gallery__item__remove_img">&times;</span>' +
			'</div>';
		$(this).next(".my__gallery__wrap").prepend(gallery_item);
		return false;
	});
	/* End MyCustomGallery */
		
		
	/* save_my_checkbox ajax */
	$(".save_my_checkbox").change( function(){
		var value = $(this).is(":checked");
		var key   = $(this).attr("data-key");
		var id    = $(this).attr("data-id");

		var data = {
			action: 'save_checkbox',
			value: value,
			key: key,
			id: id
		};

		$.ajax({
			url: ajaxurl,
			type: "POST",
			data: data,
			success: function(data) {
				console.log(data);
			},
		});
	return false;
	});
	/* End save_my_checkbox ajax */

	/* AddFile */
	$('.startwp__add_file__btn').live('click',function () {
		var send_attachment_bkp = wp.media.editor.send.attachment;
		var button = $(this);
		wp.media.editor.send.attachment = function (props, attachment) {
			$(button).prev().attr('value', attachment.url);
			wp.media.editor.send.attachment = send_attachment_bkp;
		};
		wp.media.editor.open(button);
		return false;
	});
	/* End AddFile */

	/* CustomFieldConstruct */
  $('.custom_field_construct__btn').live('click',function () {
      var main_wrap = $(this).parent('.custom_field_construct');
      var item_wrap = main_wrap.find(".custom_field_construct__item_wrap");
      
      var max = 0;

      // find max id
      $(".custom_field_construct__item").each( function (){
        var num = $(this).attr("data-id");
        if ( num >= max ) {
          max = num;
          max++;
        }
      });

      var template =
        '<div class="custom_field_construct__item" data-id="' + max + '">'+
          '<span class="custom_field_construct__remove_item button button-primary button-large">X</span>'+
          '<table>'+
            '<tr>'+
              '<td>'+
                '<div class="custom_field_construct__item_img_wrap">'+
                  '<img src="" class="custom_field_construct__img" alt="NO IMAGES" />' +
                  '<input type="hidden" name="custom_field[' + max + '][image1]"  class="custom_field_construct__img_input">'+
                '</div>'+
              '</td>'+
              '<td>'+
                '<input type="text" name="custom_field[' + max + '][day]"  >'+
                '<input type="text" name="custom_field[' + max + '][data1]" >'+
                '<input type="text" name="custom_field[' + max + '][data2]" >'+
              '</td>' +
              '<td>'+
                '<textarea name="custom_field[' + max + '][text]" ></textarea>'+
              '</td>'+
            '</tr>'+
          '</table>' +
        '</div>';

      //item_wrap.append(template);
      item_wrap.prepend(template);
  });

  $(".custom_field_construct__item_img_wrap").live('click',function () {
    var send_attachment_bkp = wp.media.editor.send.attachment;
    var img_wrap = $(this);
    wp.media.editor.send.attachment = function (props, attachment) {
      img_wrap.find(".custom_field_construct__img").attr('src', attachment.url);
      img_wrap.find(".custom_field_construct__img_input").val(attachment.id);
      wp.media.editor.send.attachment = send_attachment_bkp;
    };
    wp.media.editor.open(img_wrap);
    return false;
  });

  $(".custom_field_construct__remove_item").live('click',function () {
    $block = $(this);
    if(confirm('Удалить?')) {
      $block.parent().remove();
    }
  });
  /* END CustomFieldConstruct */

	$(".qqqqqqqqqq").select2({
		maximumSelectionLength: 4
	});

   
});