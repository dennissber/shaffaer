<?php


// hidden from index name='robots'
add_action( 'init', 'action_function_name' );
function action_function_name() {
	// скрываем от инденсации
	if( isset( $_GET['sort_by'] ) ) {
		add_action( 'wp_head', 'type_archive_descr' );
		function type_archive_descr() {
			return print "<meta name='robots' content='noindex,follow' />";
		}
	}
}


#### Theme support
if (function_exists('add_theme_support')) {
	add_theme_support('menus');
	add_theme_support('post-thumbnails');
	add_theme_support('title-tag'); // выводит title в head используя wp_get_document_title() 
}


#### Register menu
add_action('after_setup_theme', function() {
	register_nav_menus( array(
		'main_menu'   => 'Основное меню',
		'footer_menu' => 'Меню в подвале',
	));
});


#### Register sidebars
add_action( 'widgets_init', 'imagmag_themepacific_widgets_init' );
function imagmag_themepacific_widgets_init() {
 	register_sidebar(array(
		'name'         => 'DefaultName',
		'before_title' => '<h2 class="widgettitle">',
		'after_title'  => '</h2>',
	));
}


#### Add image size // plagin Force Regenerate Thumbnails 
if ( function_exists( 'add_image_size' ) ) {
  add_image_size( '100_100', 100, 100, true ); 
  add_image_size( '150_150', 150, 150, true ); 
	add_image_size( '230_230', 230, 230, true ); 
	add_image_size( '475_475', 475, 475, true ); 
}


#### Разрешить следующие image size
add_filter('intermediate_image_sizes', 'true_supported_image_sizes');
function true_supported_image_sizes( $sizes) {
	return array('100_100', '230_230', '475_475' );
}


#### Добавить свои image size в меню admin 
add_filter( 'image_size_names_choose', 'my_custom_sizes' );
function my_custom_sizes( $sizes ) {
  return array_merge( $sizes, array(
    '1300_1000' => '1300_1000',
    '1200_300'  => '1200_300',
  ) );
}



#### Подключение скриптов и стилей https://truemisha.ru/blog/wordpress/wp_enqueue_script.html
## Условные теги http://wp-kama.ru/id_89/uslovnyie-tegi-v-wordpress-i-vse-chto-s-nimi-svyazano.html
add_action( 'wp_enqueue_scripts', 'my_scripts_method' );
function my_scripts_method() {
  if( !is_admin() ){

  	## Общие стили
    wp_register_style( 'main_style', ABS_ASSETS_URI.'/css/style.css',  array(), '1.0.0', null );

    ## переподключаем jQuery
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', ABS_ASSETS_URI.'/js/jquery-3.2.1.min.js', false, null, true );

    ## регистрация доп. скриптов и стилей
  	wp_register_style ( 'formstyler', ABS_ASSETS_URI.'/css/jquery.formstyler.css', array(), null, null );
    wp_register_script( 'formstyler', ABS_ASSETS_URI.'/js/jquery.formstyler.min.js', array('jquery'), null, true );

  	wp_register_style ( 'slick_slider', ABS_ASSETS_URI.'/libs/slick_slider/slick/slick.min.css', array(), null, null );
    wp_register_script( 'slick_slider', ABS_ASSETS_URI.'/libs/slick_slider/slick/slick.min.js', array('jquery'), null, true );


    wp_register_script( 'maskedinput_js', ABS_ASSETS_URI.'/js/maskedinput/jquery.maskedinput.min.js', array('jquery'), null, true );
    wp_register_script( 'common-js', ABS_ASSETS_URI.'/js/main.js', array('jquery'), null, true );

    ## for all page
    wp_enqueue_style('main_style');
    wp_enqueue_script( 'jquery' );

    if ( !is_front_page() ){
			wp_enqueue_style ('formstyler');
			wp_enqueue_script('formstyler');
    }

    if ( is_singular( 'product' ) ){
	    wp_enqueue_style ('slick_slider');
	    wp_enqueue_script('slick_slider');
    }
    
    wp_enqueue_script( 'maskedinput_js');
    wp_enqueue_script( 'common-js');
   
  }   
}


#### Path
define('REL_ASSETS_URI', 'wp-content/themes/'.get_template().'/assets/'); // для изображений
define('ABS_ASSETS_URI', get_template_directory_uri().'/assets'); // для скриптов и стилей
define('INC_URI', get_template_directory_uri().'/includes/');
define('INC_DIR', get_template_directory().'/includes/');


#### Plugins
// "tinymc"
require_once INC_DIR.'plugins/tinymc/tinymc_init.php';
// "wps__cart"
require_once INC_DIR.'plugins/wps_cart/wps_cart_init.php';
// "wp_clear-head"
require_once INC_DIR.'plugins/wp_clear/wp_clear.php';
// "wps_send_email"
require_once INC_DIR.'plugins/wps_send_email/wps_mail_init.php';

#### Class
require_once INC_DIR.'common.php';
require_once INC_DIR.'other/theme_settings.php';
require_once INC_DIR.'other/theme_console.php';
require_once INC_DIR.'other/WPS__TermFields.php';
// Post Types
require_once INC_DIR.'post_type/product.php';
require_once INC_DIR.'post_type/blog.php';


#### Init
new Common();
new ThemeSettings();
new Console();
new WPS_Mail();
// PostType
new Product();
new Blog();

$WPS_Cart = new WPS_Cart();

// TermFields
new WPS__TermFields( 
  array(
    'taxonomy'  => array( 'ingredients' ),
    'fields'    => array(
      array(
        'f_name' => 'image',
        'type'   => 'image',
        'title'  => 'Изображение на превью',
        'desc'   => '',
      ),
      array(
        'f_name' => 'seo_text_top',
        'type'   => 'wp_editor',
        'title'  => 'Контент',
        'desc'   => '',
      ),
    )
  )
);


// TermFields
new WPS__TermFields( 
  array(
    'taxonomy'  => array( 'product_cat' ),
    'fields'    => array(
      array(
        'f_name' => 'seo_title',
        'type'   => 'input',
        'title'  => 'H1',
        'desc'   => 'СЕО загловок H1 для страниц категорий.',
      ),
      array(
        'f_name' => 'seo_text_top',
        'type'   => 'wp_editor',
        'title'  => 'SEO Text верхний',
        'desc'   => 'СЕО текст для страниц категорий.',
      ),
      array(
        'f_name' => 'seo_text_bottom',
        'type'   => 'wp_editor',
        'title'  => 'SEO Text нижний',
        'desc'   => 'СЕО текст для страниц категорий.',
      )
    )
  )
);

// TermFields
new WPS__TermFields( 
  array(
    'taxonomy'  => array( 'product_properties' ),
    'fields'    => array(
      array(
        'f_name' => 'seo_title',
        'type'   => 'input',
        'title'  => 'Title',
        'desc'   => 'Title для страниц свойств.',
      ),
      array(
        'f_name' => 'seo_text_top',
        'type'   => 'wp_editor',
        'title'  => 'SEO Text верхний',
        'desc'   => 'СЕО текст для страниц свойств.',
      ),
    )
  )
);


## SET TITLE
add_action( 'pre_get_document_title', 'wps_set_seo_title', 99999 );
function wps_set_seo_title(){
    return "qwdwq";

  // if is page taxonomy
  if ( is_tax() ){
    return "qwdwq";
    $cur_cat_obj  = get_queried_object();
    $term_id      = $cur_cat_obj->term_id;
    $seo_title    = get_term_meta( $term_id, 'wps_seo__title', true );
    if ( $seo_title ){
      return $seo_title;
    } 
  }

}



#### Определение кол-ва постов на странице
add_action('pre_get_posts','custom_posts_per_page');
function custom_posts_per_page($query){
	// Выходим, если это админ-панель или не основной запрос.
	if ( is_admin() || ! $query->is_main_query() )
		return;
	// Если тип архива "custom_post"
	if ( is_post_type_archive( "custom_post" ) ){
		$query->set('posts_per_page',2);
	}
	// Если тип таксономии "name_cat"
	if ( !is_post_type_archive( "custom_post" ) && is_taxonomy_hierarchical( "name_cat" ) ){ 
		$query->set('posts_per_page',1);
	}
}
// удаляет H2 из шаблона стандартной пагинации
add_filter('navigation_markup_template', 'my_navigation_template', 10, 2 );
function my_navigation_template( $template, $class ){
	return '
	<nav class="navigation %1$s" role="navigation">
		<div class="nav-links">%3$s</div>
	</nav>    
	';
}



// filter_by_type
function filter__show_by_type( $type_id ) {
  $args['tax_query'] = array('relation' => 'AND');
	if ($type_id != '') { 
    $args['tax_query'][] =  array( 
      array( 
        'taxonomy' => 'product_type', 
        'field'    => 'id', 
        'terms'    => $type_id
      ) 
    );
  }
  return $args;
}

// filter_sort_by
function filter__sort_by( $sort_by ) {
	$args = array( );

	if ( $sort_by == "DESC" || $sort_by == "ASC" ) {
		$args = array( 
			'meta_key' => 'prod_price',
      'orderby'  => 'meta_value_num',
      'order'    => $sort_by
		);
	}

	if ( $sort_by == "new" ) {
		$args = array( 
      'orderby'  => 'date',
      'order'    => 'DESC'
		);
	}

	if ( $sort_by == "popular" ) {
		$args = array( 
      'meta_key' => 'post_views_count',
      'orderby'  => 'meta_value_num',
      'order'    => 'DESC',
		);
	}

	return $args;
}

// go_filter
function go_filter( $array ){
	return query_posts($array);
}

## remove_comment_fields
function remove_comment_fields($fields) {
  unset($fields['url']);
  return $fields;
}
add_filter('comment_form_default_fields','remove_comment_fields');




// https://truewp.ru/blog/wordpress/taksonomii-v-url-tipov-postov.html
// http://omurashov.ru/wordpress_rewrite_rules/
// переписываем ЦПУ для product
// удаляем слаг типа поста из url
// https://wp-kama.ru/function/wp_rewrite
// работает при 'rewrite' => true,
add_filter('post_type_link', 'remove_post_type_slug', 10, 2 );
add_action('pre_get_posts', 'add_post_type_to_get_posts_request' );

// Удаляем префикс с именем типа записи из URL
function remove_post_type_slug( $post_link, $post ){
  if( $post->post_type === 'product' ){
    return str_replace("/$post->post_type/", '/', $post_link );
  }
  return $post_link;
}

// Добавляем тип записи в запрос
function add_post_type_to_get_posts_request( $query ){
  if( ! $query->is_main_query() ) return; // не основной запрос
  // не запрос с name параметром (как у постоянной страницы)
  if( ! isset($query->query['page']) || empty($query->query['name']) || count($query->query) != 2 )
    return;
  $query->set('post_type', array('post', 'page', 'product') ); 
}

 



#### Theme Function 
require_once INC_DIR.'theme_functions.php';