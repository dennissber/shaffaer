<div class="container_out">
  <div class="col col-xxs-6 col-md-6 col-xl-3">
    <a class="breadcrumbs_category" href="uhod-za-telom/" style="background-image:url(<?= REL_ASSETS_URI; ?>images/categories/1.jpg)">
      <div class="breadcrumbs_category_title">
        Уход за телом
      </div>
    </a>
  </div>
  <div class="col col-xxs-6 col-md-6 col-xl-3">
    <a class="breadcrumbs_category" href="uhod-za-volosami/" style="background-image:url(<?= REL_ASSETS_URI; ?>images/categories/2.jpg)">
      <div class="breadcrumbs_category_title">
        Уход за волосами
      </div>
    </a>
  </div>
  <div class="col col-xxs-6 col-md-6 col-xl-3">
    <a class="breadcrumbs_category" href="uhod-za-licom/" style="background-image:url(<?= REL_ASSETS_URI; ?>images/categories/3.jpg)">
      <div class="breadcrumbs_category_title">
        Уход за лицом
      </div>
    </a>
  </div>
  <div class="col col-xxs-6 col-md-6 col-xl-3">
    <a class="breadcrumbs_category" href="naturalnaya-zubnaya-pasta/" style="background-image:url(<?= REL_ASSETS_URI; ?>images/categories/4.jpg)">
      <div class="breadcrumbs_category_title">
        Зубная паста
      </div>
    </a>
  </div>
</div>