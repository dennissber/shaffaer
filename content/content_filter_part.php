 
<?php 
  ## print children categoty

  $tax_name    = 'product_cat';

  // data of cur term
  $cur_terms   = get_queried_object();
  $cur_tax_id  = $cur_terms->term_id;
  $has_parent  = $cur_terms->parent; // cur_terms parent or child ? get num tax or null
  $parent_link = get_category_link( $has_parent );

  // если текущая таксономия имеет родителя - она дочерняя, значит, берем id родителя и получаем его дочерние элементы
  // если текущая таксономия не имеет родителя - она родитель, значит, берем текущий id и получаем дочерние элементы
  $result_id   = $has_parent ? $has_parent : $cur_tax_id;
  $cur_child   = get_term_children( $result_id, $tax_name );

  ## sort by
  global $sort_by;
  $sort_by = htmlspecialchars( $_GET['sort_by'] );
?>

 <section class="filter">
    <div class="open_mobile_filter"></div>
    <div class="wrapper">
      <div class="container_out">

        <div class="col col-xl-3">
        <?php if ( $cur_child ) : ?>
          <span>
            Тип:
          </span>
          <select name="type" class="pt_sel1" onChange="window.location.href=this.value" >
            <option value="<?php echo $parent_link; ?>">Все</option>
              <?php 
                foreach( $cur_child as $term ) {
                  $terms      = get_term_by( 'id', $term, 'product_cat' );
                  $term_count = $terms->count;
                  $term_id    = $terms->term_id;
                  $term_name  = get_term_meta( $term_id, 'seo_title', true );
                  $term_link  = get_category_link( $term_id );
                  if ( $term_count != 0 ) {
                    echo '<option '.( $term_id == $cur_tax_id ? 'selected="selected"' : '' ).' value="' . $term_link . '">' . $term_name ." (".$term_count.") ". '</option>';
                  }
                }
              ?>
          </select>
        <?php endif; ?>
        </div>

        <div class="col col-xl-5">
          <span>
            Свойство:
          </span>

          <select name="type" class="pt_sel1" onChange="window.location.href=this.value" >
            <option value="<?php echo $parent_link; ?>">Все</option>
              <?php 
              $args = array(
                'taxonomy'   => 'product_properties',
                'hide_empty' => true,
                'orderby'    => 'name'
              );
              $terms = get_terms( $args );

              if ( $terms ){

                foreach( $terms as $terms ) {
                  $term_count = $terms->count;
                  $term_id    = $terms->term_id;
                  $term_name  = $terms->name;
                  $term_link  = get_category_link( $term_id );
                  if ( $term_count != 0 ) {
                    echo '<option '.( $term_id == $cur_tax_id ? 'selected="selected"' : '' ).' value="' . $term_link . '">' . $term_name ." (".$term_count.") ". '</option>';
                  }
                }
              }
              ?>
          </select>


        </div>

        <form>
        <div class="col col-xl-4 filter__right">
          <span>
            Сортировка:
          </span>
          <select name="sort_by" class="pt_sel">
            <option <?php echo $sort_by == "new"     ? 'selected="selected"' : ''; ?> value="new"     >Новинки</option>
            <option <?php echo $sort_by == "ASC"     ? 'selected="selected"' : ''; ?> value="ASC"     >По цене: сначала дешевые</option>
            <option <?php echo $sort_by == "DESC"    ? 'selected="selected"' : ''; ?> value="DESC"    >По цене: сначала дорогие</option>
            <option <?php echo $sort_by == "popular" ? 'selected="selected"' : ''; ?> value="popular" >Популярные</option>
          </select>
        </div>
        </form>
      </div>
    </div>
  </section>