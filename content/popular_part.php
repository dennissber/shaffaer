<div class="wrapper">
  <div class="container_out">

    <?php
    $args_loop = array( 
      'post_type'      => 'product',
      'posts_per_page' => 4,
      'meta_query'     => array(
        array(
          'key'   => 'prod_popular',
          'value' => 'on'
        )
      ),
    );
    $custom_loop = new WP_Query( $args_loop );

    while ( $custom_loop->have_posts() ) : $custom_loop->the_post();

      $miniature     = get_post_meta($post->ID, "miniature", true);
      $miniature_url = wp_get_attachment_url( $miniature, '230_230' );
    ?>

      <div class="col col-xxxs-6 col-xs-6 col-md-3 col-xl-3">
        <a class="product_preview resizeTo1x1" href="<?php the_permalink(); ?>" style="background-image:url(<?= $miniature_url; ?>); ">
          <div class="read_more">
            Подробнее
          </div>
        </a>
      </div>
      
    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>

  </div>
</div>