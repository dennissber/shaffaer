<!DOCTYPE html>
<html <?php language_attributes(); ?> >
  <head>
    <meta charset="utf-8">

    <!-- base url -->
    <base href="<?php echo get_site_url( ); ?>/" > 
    
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <meta name="theme-color" content="#DFF4C6">

    <link rel="icon" href="<?= REL_ASSETS_URI; ?>images/favicon.ico">

    <?php wp_head(); ?>

    <?php // drop Google Analytics Here ?>
    <?php // end analytics ?>
  </head>
  <body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

  <?php 
  $phone1    = get_option( "theme_option_contact_phone_1" );
  $phone2    = get_option( "theme_option_contact_phone_2" );
  $schedule1 = get_option( "theme_option_contact_schedule_1" );
  $schedule2 = get_option( "theme_option_contact_schedule_2" );
  ?>

  <!-- header --> 
  <div id="header">
    <div class="wrapper">
      <div class="container">
        <div class="logo">
          <a href="">
            <img alt="logo" src="<?php echo REL_ASSETS_URI; ?>images/logo.png">
          </a>
          <div class="descriptor">
            Натуральная косметика из Индии
          </div>
        </div>
        <div class="header_info">
          <div class="phones">
            <span>
              <?= $phone1; ?>
            </span>
            <span>
              <?= $phone2; ?>
            </span>
          </div>
          <div class="working_time">
            <span>
            	<?= $schedule1; ?>
            </span>
            <span>
            	<?= $schedule2; ?>
            </span>
          </div>
          <div class="cart">
            <a href="cart">
              <div class="icon"></div>
            </a>
            <span class="title">
              <a href="cart">
                Корзина 
              </a>
            </span>
            <span class="total">
              (<span class="cart_counter_text"><?php echo cartPriceCount(); ?></span> грн) 
            </span>
          </div>
        </div>
        <div class="header_menu">
          <div class="open_mobile_menu"></div>
          <ul>
            <li>
              <a class="dropdown_nav__btn">Каталог</a>
              <ul class="dropdown_nav" id="dropdown_nav">
                <li><a href="uhod-za-telom/">Уход за телом</a></li>
                <li><a href="uhod-za-volosami/">Уход за волосами</a></li>
                <li><a href="uhod-za-licom/">Уход за лицом</a></li>
                <li><a href="naturalnaya-zubnaya-pasta/">Зубная паста</a></li>
              </ul>
            </li>
            <li>
              <a href="about">
                О нас
              </a>
            </li>
            <li>
              <a href="contacts">
                Контакты
              </a>
            </li>
            <li>
              <a href="blog">
                Блог
              </a>
            </li>
            <li>
              <a href="ingredients">
                Ингредиенты
              </a>
            </li>
            <li>
              <a href="promo">
                Акции
              </a>
            </li>
          </ul>
        </div>
        <div class="header_search">
          <form method="get" class="searchform" action="<?php echo home_url(); ?>/">
          	<input class="header_search__text" name="s" id="s" placeholder="Поиск" type="text">
          	<input class="header_search__submit" type="submit" value="">
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- end header -->

