<?php get_header(); 

$miniature  = get_post_meta( $post->ID, "miniature", true );
$prod_type  = get_post_meta( $post->ID, "prod_type", true );
$prod_price = get_post_meta( $post->ID, "prod_price", true );

$prod_price_action = get_post_meta( $post->ID, "prod_price_action", true );

$prod_descrip     = get_post_meta( $post->ID, "prod_descrip", true );
//$prod_composition = get_post_meta( $post->ID, "prod_composition", true );
$prod_howuse      = get_post_meta( $post->ID, "prod_howuse", true );

$custom_field     = get_post_meta( $post->ID, "custom_field", true );

$pay_an_delivery  = get_option('theme_option_item_page_pay_an_delivery');
$product_gallery  = wps__get_gallery( $post->ID, 'product_gallery' );

?>

<section class="breadcrumbs">
    <div class="wrapper">
      <div class="container">
        <div class="col" id="path">

          <a href="">
            Главная
          </a>
          <span class="separator">
            &#8250;
          </span>
					<?php
						$cur_terms      = get_the_terms( get_the_ID(), 'product_cat' );

						$cur_terms_id   = $cur_terms[0]->term_id;
            $cur_terms_name = get_term_meta( $cur_terms_id, 'seo_title', true );
            $cur_terms_link = get_category_link( $cur_terms_id );

            if ( !empty( $cur_terms ) ){
  						foreach ( $cur_terms as  $value ) {
  							if ( $value->parent != 0 ) {
  								$cur_terms_id   = $value->term_id;
  								$cur_terms_name = get_term_meta( $cur_terms_id, 'seo_title', true );
              		$cur_terms_link = get_category_link( $cur_terms_id );

  								$par_terms_id   = $value->parent;
  								$par_terms_name = get_term_meta( $par_terms_id, 'seo_title', true );
              		$par_terms_link = get_category_link( $par_terms_id );

              		echo "<a href='{$par_terms_link}' >{$par_terms_name}</a>";
  								echo "<span class='separator' >&#8250;</span>";
                  // выйти после первого выполнения условия, чтобы избежать дублирования заголовска род. категории
                  break;
  							}
  						}
            }

						echo "<a href='{$cur_terms_link}' >{$cur_terms_name}</a>";
						echo "<span class='separator' >&#8250;</span>";
					?>
          <span>
            <?php the_title(); ?>
          </span>
        </div>
   
      </div>
    </div>
  </section>
  <section class="product_card_wrapper">
    <div class="wrapper">
      <div class="container">
        <div class="product_title col col-md-7 left_side">
          <h1>
            <?php the_title(); ?>
          </h1>
        </div>
        <div class="col col-md-5 left_side">
          <div class="gallery_preview resizeTo1x1" >
            <?php
            if ( !empty($product_gallery[0] ) ){ 
              foreach ($product_gallery as $value) { ?>
              <div class="gallery_preview__item">
                <?php echo wp_get_attachment_image( $value, "475_475" ); ?>
              </div>
            <?php 
              }
            } else {
              echo wp_get_attachment_image( $miniature, "475_475" );
            }
            ?>
          </div>
        </div>



  
  <?php 
  /* micro data */
  $image_url = wp_get_attachment_url( $miniature, "475_475" );

  ?>



<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Product",
  "name": "<?= the_title(); ?>",
  "image": "<?= $image_url; ?>",
  "description": "<?= $prod_descrip; ?>",
  "offers": [
    {
      "@type": "Offer",
      "priceCurrency" : "UAH",
      "price" : "<?= $prod_price; ?>"
    }
  ]
}
</script>




        <div class="col col-md-7 right_side">
          <div class="container_out">
            <div class="product_controls">
              <div class="col col-xl-4">
                <p>
                  <span>
                    Тип
                  </span>
                  <span>
                    <?= $prod_type; ?>
                  </span>
                </p>
                <!-- price -->
                <?php if ( $prod_price_action ) : ?>
                <p class="single_old_price">
                  <span>
                    Цена
                  </span>
                  <span>
                    <?= $prod_price; ?> грн
                  </span>
                </p>
                <p class="single_action_price">
                  <span>
                    Цена
                  </span>
                  <span>
                    <?= $prod_price_action; ?> грн
                  </span>
                </p>
                <?php else: ?>
                  <p>
                    <span>
                      Цена
                    </span>
                    <span>
                      <?= $prod_price; ?> грн
                    </span>
                  </p>
                <?php endif; ?>
                <p>
                  <span>
                    В наличии
                  </span>
                </p>
                <!-- end price -->
              </div>
              <div class="col col-xl-4">
                <button class="buy_now" data-id="<?php echo get_the_ID(); ?>">
                  Купить в 1 клик
                </button>
              </div>
              <div class="col col-xl-4">
                <button class="read_more item_buy_btn" data-id="<?php echo $post->ID; ?>">
                  Добавить в корзину
                </button>
              </div>
            </div>
          </div>
          <div class="tabs">
            <div class="tab_titles">
              <div class="tab_title active">
                <h2>Описание</h2>
              </div>
              <div class="tab_title">
                <h2>Состав</h2>
              </div>
              <div class="tab_title">
                <h2>Свойства</h2>
              </div>
              <div class="tab_title">
                <h2>Как пользоваться</h2>
              </div>
              <div class="tab_title">
                <h2>Отзывы</h2>
              </div>
              <div class="tab_title">
                <h2>Оплата и доставка</h2>
              </div>
            </div>
            <div class="tab_block active">
              <?= $prod_descrip; ?>
            </div>
            <div class="tab_block">
              <?php
                $ingr_list = get_the_terms( $post->ID, 'ingredients' );
                //pre_print_r( $ingr_list );
                if ( $ingr_list ){
                  echo '<ul class="tovar_ingredients_list">';
                  foreach( $ingr_list as $cur_term ){
                    $termId = $cur_term->term_id;
                    $link   = get_category_link( $termId );
                    $name   = $cur_term->name;
                    echo '<li><a href="'.$link.'">'.$name.'</a></li>';
                  }
                  echo "</ul>";
                }
              ?>
            </div>
            <div class="tab_block">
              <?php
                $ingr_list = get_the_terms( $post->ID, 'product_properties' );
                //pre_print_r( $ingr_list );
                if ( $ingr_list ){
                  echo '<ul class="tovar_ingredients_list">';
                  foreach( $ingr_list as $cur_term ){
                    $termId = $cur_term->term_id;
                    $link   = get_category_link( $termId );
                    $name   = $cur_term->name;
                    echo '<li><a href="'.$link.'">'.$name.'</a></li>';
                  }
                  echo "</ul>";
                }
              ?>
            </div>
            <div class="tab_block">
              <?= $prod_howuse; ?>
            </div>
            <div class="tab_block">
              <?php comments_template(); ?>
            </div>
            <div class="tab_block" id="empty_hidden">
              <?php if(!empty($_GET['hidden'])) echo '<div id="hidden">' . wpautop( $pay_an_delivery) . '</div>'; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="wrapper">
      <div class="container">
        <h3 class="category_title single_category_title">
          С этим товаром покупают 
        </h3>
      </div>
    </div>
    <div class="wrapper">
      <div class="container_out">
        <?php 
        if ( $custom_field ) :
        foreach ($custom_field["select_tov"] as  $value) {
          $the_post      = get_post( $value );
          $post_title    = $the_post->post_title;
          $miniature     = get_post_meta( $the_post->ID, 'miniature', true );
          $miniature_url = wp_get_attachment_url( $miniature );
          $post_link     = get_permalink( $the_post->ID );
        ?>

        <div class="col col-xxxs-6 col-xs-6 col-md-3 col-xl-3">
          <div class="product_preview__wrap">
            <a class="product_preview resizeTo1x1" href="<?= $post_link; ?>" style="background-image:url(<?= $miniature_url; ?>)">
              <div class="read_more">
                Подробнее
              </div>
            </a>
            <a href="<?= $post_link; ?>" class="product_preview__guid" ><?php echo $post_title; ?></a>
          </div>
        </div>

        <?php
        }
        endif;
        ?>

      </div>
    </div>
  </section>

<?php setPostViews( get_the_ID() ); ?>
  
<?php get_footer(); ?>
