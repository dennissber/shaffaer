<?php
	/**
	 * Template Name: Страница благодарности
	 */
	get_header();
?>

<section>
    <div class="wrapper">
      <div class="container">
        <h1 class="category_title">
          Спасибо за Ваш заказ
        </h1>
      </div>
    </div>
  </section>
  <section>
    <div class="wrapper">
      <div class="container">

        <!--
        <div class="checkout_results">
          <div class="checkout_total_results">
            <p class="checkout_title">
              № Вашего заказа
              <span>
                148614618
              </span>
            </p>
            <ul>
              <li class="checkout_total_item">
                <a href="/product_card">
                  Шампунь «Алое вера»
                </a>
                <div class="value">
                  <span>
                    3
                  </span>
                  <span>
                    шт.
                  </span>
                </div>
              </li>
              <li class="checkout_total_item">
                <a href="/product_card">
                  Шампунь «Алое вера»
                </a>
                <div class="value">
                  <span>
                    3
                  </span>
                  <span>
                    шт.
                  </span>
                </div>
              </li>
            </ul>
          </div>
          <div class="checkout_total_contacts">
            <p>
              ФИО: Иванов Иван
            </p>
            <p>
              Город: Горишние Плавни
            </p>
            <p>
              Способ доставки: Самовывоз
            </p>
            <p>
              Способ оплаты: Наличными
            </p>
          </div>
          <div class="checkout_total_price">
            <p>
              Итого к оплате:
              <span class="value">
                10500
              </span>
              <span class="currency">
                грн.
              </span>
            </p>
          </div>
        </div>
        -->


        <div class="subscribe_form">
          <p>
            Ваш заказ принят оператором. Мы свяжемся с Вами, когда заказ будет готов к отправке.
            <br><br>
            <!--
            Сообщение о подтверждении заказа было выслановам Вам на телефон.
            -->
          </p>
          <h3>
            Будьте в курсе наших новостей
          </h3>
          <p>
            Получайте рассылку с нашими новостями и акциями на почту
          </p>
          <form class="subscribe_form">
            <input placeholder="Ваш e-mail" type="email" name="user_email">
            <input type="hidden" name="action" value="subscribe_form">
            <input type="submit" value="Подписаться">
          </form>
        </div>
      </div>
    </div>
  </section>


  <section>
    <div class="wrapper">
      <div class="container">
        <h3 class="category_title">
          Популярные товары
        </h3>
      </div>
    </div>
    <div class="wrapper">
      <?php get_template_part( 'content/popular_part' ); ?>
    </div>
  </section>
 


<?php get_footer(); ?>