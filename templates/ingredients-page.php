<?php
	/**
	 * Template Name: Ингредиенты
	 */
	get_header();
?>

 <section class="breadcrumbs">
    <div class="wrapper">
      <div class="container">
        <div class="col" id="path">
          <a href="">
            Главная
          </a>
          <span class="separator">
            &#8250;
          </span>
          <span>
            Ингредиенты
          </span>
        </div>
      </div>
    </div>
  </section>

  <section class="ingredients_archive">
    <div class="wrapper">
      <div class="row">
        <?php 

        // получить все термины указанной таксономии
        // скрывать пустые
        // сортировать по имени
        $args = array(
          'taxonomy'   => 'ingredients',
          'hide_empty' => true,
          'orderby'    => 'name'
        );
        $terms = get_terms( $args );
        //pre_print_r( $terms);

        foreach ($terms as $value) { 
          $termId = $value->term_id;
          $img    = get_term_meta( $termId, 'image', true );
          $link   = get_category_link( $termId );

        ?>

        <div class="col col-xxs-6 col-md-6 col-xl-3">
          <div class="ingredients__item">
            <a href="<?= $link; ?>" class="avatar">
            <span class="ingredients__item__title">
             <?= $value->name; ?>
            </span>
            <?php echo wp_get_attachment_image( $img, '230_230' ); ?>
            </a>
          </div>
        </div>
        <?php
        }
        ?>
       </div>

    </div>
  </section>


<?php get_footer(); ?>