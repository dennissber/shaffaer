<?php
	/**
	 * Template Name: Корзина
	 */
	get_header();
?>



  <section class="breadcrumbs">
    <div class="wrapper">
      <div class="container">
        <div class="col" id="path">
          <a href="">
            Главная
          </a>
          <span class="separator">
            &#8250;
          </span>
          <span>
            Корзина
          </span>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="wrapper">
      <div class="container">
        <h3 class="category_title">
          Корзина
        </h3>
      </div>
    </div>
  </section>
  <div class="section">
    <div class="wrapper">
      <div class="container">


        <?php if ( cartPriceCount() != 0 ) : ?>
        <?php 

          $product = $WPS_Cart->cartCallProduct();
          //pre_print_r( $product );
          
          foreach( $product as $item ){ 
          	//pre_print_r( $item );
            $item_ID      = $item->ID;
            $item_title   = $item->post_title;
            $item_link    = get_permalink( $item_ID );
            $item_count   = $item->count;
            $item_uniqId  = $item->uniqId;
            $prod_price   = $item->price; 
            $miniature    = get_post_meta($item_ID, "miniature", true); 
          ?>

        <div class="cart_item" >
          <div class="cart_item_check">
            <!--<input checked="" name="check" type="checkbox">-->
            <span class="cart_item__remove" data-uniId="<?= $item_uniqId; ?>" >x</span>
          </div>
          <a class="product_preview resizeTo1x1" href="<?= $item_link; ?>" style="background-image:url('<?= wp_get_attachment_url( $miniature, '150_150' ); ?>')">
            <div class="read_more">
              Смотреть товар
            </div>
          </a>
          <div class="cart_item_title">
            <a href="<?= $item_link; ?>">
              <?= $item_title; ?>
            </a>
          </div>
          <div class="cart_item_quantity">
            <input max="500" min="1" class="cart_counter_item_js" type="number"  value="<?= $item_count; ?>">
          </div>
          <div class="cart_item_price_wrapper">
            <div class="cart_item_price">
              <span class="price">
                <?= $prod_price; ?>
              </span>
              <span class="currency">
                грн
              </span>
              <span class="multiply">
                x
              </span>
              <span class="quantity" data-uniId="<?= $item_uniqId; ?>" >
                
              </span>
            </div>
            <div class="cart_item_total_price">
              <span class="price">
                
              </span>
              <span class="currency">
                грн
              </span>
            </div>
          </div>
        </div>
        <?php }?>

        <div class="cart_total">
          <div class="cart_total_price">
            <span id="total_price">
              400
            </span>
            <span id="total_price_currency">
               грн
            </span>
          </div>
          <div class="cart_total_title">
            Итого :
          </div>
        </div>
        <div class="cart_whats_next">
          <a class="back back_cart" href="catalog/">
            Продолжить покупки
          </a>
          <button class="checkout cart__update_btn">
            Оформить заказ
          </button>
        </div>

        <?php else: ?>

          <div class="cart_total">
            Коризна пуста.
          </div>

        <?php endif; ?>

      </div>
    </div>
  </div>
  <section>
    <div class="wrapper">
      <div class="container">
        <h3 class="category_title">
          Популярные товары
        </h3>
      </div>
    </div>
    <?php get_template_part( 'content/popular_part' ); ?>
  </section>

<?php get_footer(); ?>