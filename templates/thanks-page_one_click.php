<?php
	/**
	 * Template Name: Страница благодарности (купить в один клик)
	 */
	get_header();
?>

<section>
    <div class="wrapper">
      <div class="container">
        <h1 class="category_title">
          Спасибо за Ваш заказ
        </h1>
      </div>
    </div>
  </section>
  <section>
    <div class="wrapper">
      <div class="container">
        <div class="subscribe_form">
          <p>
            Ваш заказ принят оператором. Мы свяжемся с Вами в ближайшем времени.
          </p>
          <br>
          <h3>
            Будьте в курсе наших новостей
          </h3>
          <p>
            Получайте рассылку с нашими новостями и акциями на почту
          </p>
          <form class="subscribe_form">
            <input placeholder="Ваш e-mail" type="email" name="user_email">
            <input type="hidden" name="action" value="subscribe_form">
            <input type="submit" value="Подписаться">
          </form>
        </div>
      </div>
    </div>
  </section>


<?php get_footer(); ?>