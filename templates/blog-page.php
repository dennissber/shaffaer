<?php
	/**
	 * Template Name: Блог
	 */
	get_header();
?>

  <section class="breadcrumbs">
    <div class="wrapper">
      <div class="container">
        <div class="col" id="path">
          <a href="">
            Главная
          </a>
          <span class="separator">
            &#8250;
          </span>
          <span>
            Блог
          </span>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="wrapper">
      <?php
        $args_loop = array( 
          'post_type' => 'article',
          'posts_per_page' => -1,
        );
        $custom_loop = new WP_Query( $args_loop );
        while ( $custom_loop->have_posts() ) : $custom_loop->the_post(); 
        ?>

        <div class="blog__item">
          <h2 class="blog__item__title"><a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a></h2>
          
          <?php the_excerpt(); ?>
        </div>

        <?php 
        endwhile;
        wp_reset_postdata();
        ?> 
    </div>
  </section>

<?php get_footer(); ?>