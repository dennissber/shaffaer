<?php
	/**
	 * Template Name: Оформление заказа
	 */
	get_header();
?>


 <section class="breadcrumbs">
    <div class="wrapper">
      <div class="container">
        <div class="col" id="path">
          <a href="">
            Главная
          </a>
          <span class="separator">
            &#8250;
          </span>
          <a href="cart/">
            Корзина
          </a>
          <span class="separator">
            &#8250;
          </span>
          <span>
            Оформление заказа
          </span>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="wrapper">
      <div class="container">
        <h3 class="category_title">
          Оформление заказа
        </h3>
      </div>
    </div>
  </section>

  <?php if ( cartPriceCount() != 0 ) : ?>

  <div class="section">
    <div class="wrapper">
      <div class="container">
        <div class="checkout_items">
          <h4>
            Ваш заказ
          </h4>

          <?php 

          $product = $WPS_Cart->cartCallProduct();
          //pre_print_r( $product );
          
          foreach( $product as $item ){ 
            $item_ID      = $item->ID;
            $item_title   = $item->post_title;
            $item_link    = get_permalink( $item_ID );
            $item_count   = $item->count;
            $item_uniqId  = $item->uniqId;
            $prod_price   = $item->price; 
            $miniature    = get_post_meta($item_ID, "miniature", true); 
          ?>

          <div class="checkout_item">
            <div class="checkout_item_image resizeTo1x1" style="background-image:url('<?= wp_get_attachment_url( $miniature, '150_150' ); ?>')"></div>
            <div class="cart_item_title">
              <a href="<?= $item_link; ?>">
                <?= $item_title; ?>
              </a>
            </div>
            <div class="cart_item_price">
              <span class="price">
                <?= $prod_price; ?>
              </span>
              <span class="currency">
                грн
              </span>
              <span class="multiply">
                x
              </span>
              <span class="quantity">
                <?= $item_count; ?>
              </span>
            </div>
            <div class="cart_item_total_price">
              <span class="price">
                <?= $prod_price * $item_count; ?>
              </span>
              <span class="currency">
                грн
              </span>
            </div>
          </div>

          <?php } ?>

          <div class="checkout_total">
            Итого к оплате:
            <div class="price_wrapper">
              <span class="price">
                <?php echo cartPriceCount(); ?>
              </span>
              <span class="currency">
                грн
              </span>
            </div>
          </div>
        </div>
        <form class="checkout_form cart_order_form">
          <div class="container">
            <label>
              <div class="col col-md-6">
                Ваше имя
              </div>
              <div class="col col-md-6">
                <input required="" name="user_name" type="text">
              </div>
            </label>
          </div>
          <div class="container">
            <label>
              <div class="col col-md-6">
                Ваша фамилия
              </div>
              <div class="col col-md-6">
                <input required="" name="user_subname" type="text">
              </div>
            </label>
          </div>
          <div class="container">
            <label>
              <div class="col col-md-6">
                Мобильный телефон для связи с Вами
              </div>
              <div class="col col-md-6">
                <input required="" name="user_phone" type="tel">
              </div>
            </label>
          </div>

          <div class="container">
            <div class="col col-md-6">
              Способы доставки
            </div>
            <div class="col col-md-6">
              <label>
                <input name="shipping" type="radio" value="Службой доставки "Новая почта" по Украине">
                Службой доставки "Новая почта" по Украине
              </label>
              <label>
                <input name="shipping" checked type="radio" value="Самовывоз в г. Днепр">
                Самовывоз в г. Днепр
              </label>
              <label>
                <input name="shipping" type="radio" value="Курьером по г. Днепр">
                Курьером по г. Днепр
              </label>
            </div>
          </div>

          <div class="container">
            <div class="col col-md-6">
              Ваша область
            </div>
            <div class="col col-md-6">
              <?php 
                $area = simplexml_load_file(REL_ASSETS_URI.'delivery_data/area.xml');

                echo '<select id="area" name="user_area" class="order_delivery_select order_delivery_select__area" >
                  <option data-area="0" data-city="0" value="Пусто">Выберите область</option>';
                  foreach($area->data->item as $item){
                    echo '<option data-area="'.$item->Ref.'" data-city="0" value="'.$item->Description.'">'.$item->Description.'</option>';
                  }
                echo '</select>';

                unset($area);
              ?>
            </div>
          </div>

          <div class="container">
            <div class="col col-md-6">
              Ваш город
            </div>
            <div class="col col-md-6">
              <?php 
                $cities = simplexml_load_file(REL_ASSETS_URI.'delivery_data/cities.xml');

                echo '<select id="cities" name="user_city" class="order_delivery_select" >
                  <option data-area="0" data-city="0" value="Пусто">Выберите город</option>';
                  foreach($cities->data->item as $item){
                    echo '<option data-area="'.$item->Area.'" data-city="'.$item->Ref.'" value="'.$item->Description.'">'.$item->Description.'</option>';
                  }
                echo '</select>';

                unset($cities);
              ?>
            </div>
          </div>

          <div class="container">
            <div class="col col-md-6">
              Отделение Новой почты
            </div>
            <div class="col col-md-6">
              <?php 
                $warehouses = simplexml_load_file(REL_ASSETS_URI.'delivery_data/warehouses.xml');

                echo '<select id="warehouses" name="user_street" class="order_delivery_select" >
                  <option data-area="0" data-city="0" value="Пусто">Выберите отделение</option>';
                  foreach($warehouses->data->item as $item){
                    echo '<option data-area="0" data-city="'.$item->CityRef.'" value="'.$item->Description.'">'.$item->Description.'</option>';
                  }
                echo '</select>';

                unset($warehouses);
              ?>
            </div>
          </div>



          <input type="hidden" name="action" value="cart_order_form">
          <div class="container">
            <input type="submit" value="Подтвердить заказ">
          </div>
        </form>
      </div>
    </div>
  </div>
  
  <?php else: ?>
  <section>
    <div class="wrapper">
      <div class="cart_total">
        Коризна пуста.
      </div>
    </div>
  </section>
  <?php endif; ?>


<?php 
/*
error_reporting(E_ALL);
ini_set('display_errors', 1);

$area = simplexml_load_file('http://test.pengstud.com/wptest2/wp-content/themes/shaffaer/assets/qqq/area.xml');

echo '<select id="area"><option data-area="0" data-city="0" value="Пусто">Выберите область</option>';
foreach($area->data->item as $item){
  echo '<option data-area="'.$item->Ref.'" data-city="0" value="'.$item->Description.'">'.$item->Description.'</option>';
}
echo '</select>';


unset($area);

$cities = simplexml_load_file('http://test.pengstud.com/wptest2/wp-content/themes/shaffaer/assets/qqq/cities.xml');

echo '<select id="cities"><option data-area="0" data-city="0" value="Пусто">Выберите город</option>';
foreach($cities->data->item as $item){
  echo '<option data-area="'.$item->Area.'" data-city="'.$item->Ref.'" value="'.$item->Description.'">'.$item->Description.'</option>';
}
echo '</select>';


unset($cities);

$warehouses = simplexml_load_file('http://test.pengstud.com/wptest2/wp-content/themes/shaffaer/assets/qqq/warehouses.xml');

echo '<select id="warehouses"><option data-area="0" data-city="0" value="Пусто">Выберите отделение</option>';
foreach($warehouses->data->item as $item){
  echo '<option data-area="0" data-city="'.$item->CityRef.'" value="'.$item->Description.'">'.$item->Description.'</option>';
}
echo '</select>';


unset($warehouses);
*/
?>



  <section>
    <div class="wrapper">
      <div class="container">
        <h3 class="category_title">
          Популярные товары
        </h3>
      </div>
    </div>
    <div class="wrapper">
      <?php get_template_part( 'content/popular_part' ); ?>
    </div>
  </section>
  

<?php get_footer(); ?>