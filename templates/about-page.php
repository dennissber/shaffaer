<?php
	/**
	 * Template Name: О нас
	 */
	get_header();
?>


<section class="breadcrumbs">
    <div class="wrapper">
      <div class="container">
        <div class="col" id="path">
          <a href="">
            Главная
          </a>
          <span class="separator">
            &#8250;
          </span>
          <span>
            О нас
          </span>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="wrapper">
      <div class="container_out">
        <div class="col col-md-6">
          <h1>
            О нас
          </h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
            et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde
          </p>
          <p class="quote">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
            et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
            et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde
          </p>
        </div>
        <div class="col col-md-6 about_images">
          <img alt="" src="<?= REL_ASSETS_URI; ?>images/about/1.jpg">
          <img alt="" src="<?= REL_ASSETS_URI; ?>images/about/2.jpg">
          <img alt="" src="<?= REL_ASSETS_URI; ?>images/about/3.jpg">
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="wrapper">
      <div class="container_out advantages">
        <h2>
          Только натуральные компоненты
        </h2>
        <div class="col col-xs-6 col-xl-3">
          <div class="advantage_image resizeTo1x1" style="background-image:url(<?= REL_ASSETS_URI; ?>images/icons/advantages/1.png)"></div>
          <div class="advantage_description">
            Мы гарантируем качественный сертифицированный товар,
            защищенный от подделок
          </div>
        </div>
        <div class="col col-xs-6 col-xl-3">
          <div class="advantage_image resizeTo1x1" style="background-image:url(<?= REL_ASSETS_URI; ?>images/icons/advantages/2.png)"></div>
          <div class="advantage_description">
            Использование исключительно натуральные компоненты(100%),
            выращенные на собственных плантациях в чистой природной
            середе Индии
          </div>
        </div>
        <div class="col col-xs-6 col-xl-3">
          <div class="advantage_image resizeTo1x1" style="background-image:url(<?= REL_ASSETS_URI; ?>images/icons/advantages/3.png)"></div>
          <div class="advantage_description">
            Косметика является гипоаллергенной и подойдет даже для
            самой чувствительной кожи
          </div>
        </div>
        <div class="col col-xs-6 col-xl-3">
          <div class="advantage_image resizeTo1x1" style="background-image:url(<?= REL_ASSETS_URI; ?>images/icons/advantages/4.png)"></div>
          <div class="advantage_description">
            Продукция изготовлена по усовершенствованной аюрведической
            рецептуре
          </div>
        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>