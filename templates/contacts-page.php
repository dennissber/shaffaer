<?php
	/**
	 * Template Name: Контакты
	 */
	get_header();
?>

  <?php 
  $city     = get_option( "theme_option_contact_page_city" );
  $schedule = get_option( "theme_option_contact_page_schedule" );
  $phone    = get_option( "theme_option_contact_page_phone" );
  $email    = get_option( "theme_option_contact_page_email" );
  ?>



  <div class="contacts_page">
    <section class="breadcrumbs">
      <div class="wrapper">
        <div class="container">
          <div class="col" id="path">
            <a href="">
              Главная
            </a>
            <span class="separator">
              &#8250;
            </span>
            <span>
              Каталог
            </span>
          </div>
        </div>
      </div>
    </section>
    <section>
      <div class="wrapper">
        <h1>
          Контакты
        </h1>
        <div class="container_out">
          <div class="col col-md-3 col-xs-6">
            <div class="city">
              <div class="icon"></div>
              <?= $city; ?>
            </div>
          </div>
          <div class="col col-md-3 col-xs-6">
            <div class="time">
              <div class="icon"></div>
              <?= $schedule; ?>
            </div>
          </div>
          <div class="col col-md-3 col-xs-6">
            <div class="phone">
              <div class="icon"></div>
              <span class="contact_page_phones"><?= $phone; ?></span>
            </div>
          </div>
          <div class="col col-md-3 col-xs-6">
            <div class="email">
              <div class="icon"></div>
              <?= $email; ?>
            </div>
          </div>
        </div>

        <div class="container_out">
          <div class="col col-md-3"></div>
          <div class="col col-md-6">
            <form class="contact_page_form">
              <div class="contacts_form">
                <h2>
                  Есть вопросы?
                </h2>
                <p>
                  Напишите нам и мы вам ответим
                </p>
                <span>
                  Имя
                </span>
                <input type="text" name="user_name" autocomplete="off">
                <span>
                  Ваш телефон
                </span>
                <input type="tel" name="user_phone" required>
                <span>
                  Ваш вопрос
                </span>
                <input type="hidden" name="action" value="contact_page_form">
                <textarea name="user_message"></textarea>
                <input type="submit" value="Отправить">
              </div>
            </form>
          </div>
          <div class="col col-md-3"></div>
        </div>
      </div>
    </section>
  </div>
  

<?php get_footer(); ?>