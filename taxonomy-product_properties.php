<?php get_header(); ?>
 
 <section class="breadcrumbs">
    <div class="wrapper">
      <div class="container">
        <div class="col" id="path">
          <a href="">
            Главная
          </a>
          <span class="separator">
            &#8250;
          </span>
          <?php single_term_title(); ?>
        </div>
      </div>
      <?php get_template_part( 'content/category_part' ); ?>
    </div>
  </section>

  
  <section class="seo_text_wrapper ">
    <div class="wrapper">
      <h1><?php single_term_title(); ?></h1>
      <?php echo wpautop( wps__get_term_meta_field('seo_text_top') ); ?>
    </div>
  </section>

  

  <section class="ingredients_cat">
    <div class="wrapper">

      <div class="container_out">
      <?php

       while ( have_posts() ) : the_post(); 

        $miniature  = get_post_meta( $post->ID, "miniature",  true );
        $prod_type  = get_post_meta( $post->ID, "prod_type",  true );
        $prod_price = get_post_meta( $post->ID, "prod_price", true );
        $prod_price_action = get_post_meta( $post->ID, "prod_price_action", true );

        ?>

          <div class="col col-xxs-6 col-md-6 col-xl-3">
            <div class="catalog_preview">
              <div class="product_preview resizeTo1x1"  >
                <?= wp_get_attachment_image( $miniature, '230_230' ); ?>
                <a class="catalog_preview__link" href="<?php the_permalink(); ?>" ></a>
              </div>
              <a  href="<?php the_permalink(); ?>" class="title promo_title">
                <?php the_title(); ?>
              </a>
              <!-- price -->
              <div class="item_price_wrap">
                <?php if ( $prod_price_action ) : ?>
                  <div class="old_price">
                    Цена: <?= $prod_price; ?> грн
                  </div>
                  <div class="low_price">
                    <?= $prod_price_action; ?> грн
                  </div>
                <?php else: ?>
                  <div class="price">
                    Цена: <?= $prod_price; ?> грн
                  </div>
                <?php endif; ?>
              </div>
              <!-- end price -->
              <div class="buy_now" data-id="<?php echo get_the_ID(); ?>" >
                Купить в один клик
              </div>
              <a href="<?php the_permalink(); ?>" class="read_more promo_more_btn">
                Подробнее
              </a>
            </div>
          </div>

        <?php
          endwhile;
        ?> 

      </div>

    </div>
  </section>



<?php get_footer(); ?>