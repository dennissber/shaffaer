$(document).ready(function () {
    /* JQuery formstyler plugin settings */
    if (typeof $(this).styler == 'function') {
        $('select').styler();
        formStyler();
    }

    resizeBlockTo1x1();
    initTabs();
    calcucateCart();
    maskedInput();
    initModal();

    if ($(window).outerWidth() < 500 ){
        initFilterOnMobile();
        initMenuOnMobile();
    }
    if ($(window).outerWidth() > 500 ){
        pushFooterToEnd();
    }
});

$(window).resize(function () {
    resizeBlockTo1x1();

    if ($(window).outerWidth() < 500 ){
        initFilterOnMobile();
        initMenuOnMobile();
    }

    if ($(window).outerWidth() > 500 ){
        pushFooterToEnd();
    }
});

function resizeBlockTo1x1() {
    $('.resizeTo1x1').each(function () {
        $(this).css({
        height : $(this).outerWidth()
        });
    });
}

function pushFooterToEnd() {
    $('body').css({
        paddingBottom: $('#footer').outerHeight()
    });
}

function initFilterOnMobile() {
    var isOpen = true;
    openFiter();
    function openFiter() {
        if(!isOpen){
            $('.filter').addClass('open');
            $('.filter').css({
                bottom: 0
            });
            isOpen = true;
            return;
        }
        $('.filter').removeClass('open');
        $('.filter').css({
            bottom: -($('.filter').outerHeight())
        });
        isOpen = false;
    }
    $('.open_mobile_filter').click( openFiter );
}

function initMenuOnMobile() {
    var isOpen = true;
    openMenu();
    function openMenu() {
        if(!isOpen){
            $('.header_menu').addClass('open');
            $('.header_menu').css({
                bottom: 0
            });
            $(document).scroll(function (event) {
                event.preventDefault();
            })
            isOpen = true;
            return;
        }
        $('.header_menu').removeClass('open');
        isOpen = false;
    }
    $('.open_mobile_menu').click( openMenu );
}

function initTabs() {
    $('.tabs').each(function (){
        var tab = $(this);
        $(this).find('.tab_titles .tab_title').click(function (){
            $('.tabs .tab_titles .tab_title').removeClass('active');
            $(this).addClass('active');
            var active = $(this).index();
            $(tab).find('.tab_block').removeClass('active');
            $('.tab_block').eq(active).addClass('active');
        });
    });
    if ($(window).outerWidth() < 500 ){
        var tabTitlesWidth = 0;

        $('.tabs').each(function (){
            var tab = $(this);
            $(this).find('.tab_titles .tab_title').each(function () {

                $(this).css({
                    width: '60%'
                });

                $(this).css({
                    width: $(this).outerWidth()
                });

                tabTitlesWidth +=$(this).outerWidth()
                $(this).click(function () {
                    var active = $(this).index();
                    var tabWidth = $(this).outerWidth();
                    $(tab).find('.tab_titles').css({
                        marginLeft: -tabWidth * active + 50
                    })
                });

            });

            $(this).find('.tab_titles').css({
                width: tabTitlesWidth
            });
        });
    }
}

function calcucateCart() {

    $('.cart_item').each(function () {

        var cartItem = $(this);
        var anItemEnable = $(cartItem).find('.cart_item_check').find('input[type=checkbox]');

        //var anItemEnabled = ($(cartItem).find('.cart_item_check').find('input:checked')).length;
        var anItemEnabled = ($(cartItem).find('.cart_item_check')).length;

        var anItemPrice = +($(cartItem).find('.cart_item_price').find('.price').html());
        var anItemQuantity = $(cartItem).find('.cart_item_quantity').find('input[type=number]');
        var anItemQuantit = $(cartItem).find('.cart_item_price').find('.quantity');
        var totalItemPrice = $(cartItem).find('.cart_item_total_price').find('.price');

        /*
        if (!anItemEnabled){
            $(cartItem).addClass('disabled');
        } else {
            $(cartItem).removeClass('disabled');
        }
        */
        


        $(anItemQuantit).html($(anItemQuantity).val());

        var itemTotalPrice = anItemPrice * $(anItemQuantity).val();

        $(totalItemPrice).html(itemTotalPrice);

        $(anItemQuantity).keyup(function () {
            $(this).trigger('change');
            $(anItemEnable).trigger('change');
        });
        $(anItemQuantity).change(function(){

            if ($(anItemQuantity).val() < $(anItemQuantity).attr('min')){
                $(anItemQuantity).val($(anItemQuantity).attr('min'));
            } else if ($(anItemQuantity).val() > $(anItemQuantity).attr('max')) {
                // $(anItemQuantity).val($(anItemQuantity).attr('max'));
            }

            itemTotalPrice = anItemPrice * $(anItemQuantity).val();
            $(anItemQuantit).html($(anItemQuantity).val());
            $(totalItemPrice).html(itemTotalPrice);

            calculateTotal();

        })

        $(anItemEnable).change(function () {
            //anItemEnabled = ($(cartItem).find('.cart_item_check').find('input:checked')).length;
            anItemEnabled = ($(cartItem).find('.cart_item_check')).length;
            $(anItemQuantity).trigger('change');
            /*
            if (!anItemEnabled){
                $(cartItem).addClass('disabled');
            } else {
                $(cartItem).removeClass('disabled');
            }
            */
        });

    });

    calculateTotal();

    function calculateTotal() {

        var totalPrice = 0;

        $('.cart_item').each(function () {
            var anItemPrice = +($(this).find('.cart_item_price').find('.price').html());
            var anItemQuantity = +($(this).find('.cart_item_quantity').find('input').val());
            //var anItemEnabled = ($(this).find('.cart_item_check').find('input:checked')).length;
            var anItemEnabled = ($(this).find('.cart_item_check')).length;
            totalPrice += (anItemEnabled * anItemQuantity * anItemPrice);
        });

        $('#total_price').html(totalPrice);

    }

}

function formStyler() {

    $('input[type="radio"]').styler();
    $('input[type="checkbox"]').styler();
    $('input[type="number"]').styler();

}

function maskedInput() {
    $('input[type=tel]').mask("+38(999) 999-99-99");
}

function initModal() {
  $('.buy_now').click(function (e) {
    e.preventDefault();
    var id = $(this).attr("data-id");
    $(".modal_window").find(".item_id__field").val(id);
    $('.modal_wrapper').removeClass('close');
  });
  $('.modal_wrapper').find('.close_window').click(function () {
      $('.modal_wrapper').addClass('close');
  });
}


$(".pt_sel").change(function () {
  this.form.submit();
});



$(document).ready(function() {
  $('.order_delivery_select').on('change', function(){
    var firstStep = $(this).find('option:selected');

    $('.jq-selectbox__dropdown ul li').each(function(){
      if($(this).attr('data-city') == '0')
        $(this).show();
      else if($(this).attr('data-area') != 0 && $(this).attr('data-area') == firstStep.attr('data-area'))
        $(this).show();
      else if($(this).attr('data-city') == firstStep.attr('data-city'))
        $(this).show();
      else
        $(this).hide();
    });
  });
});

$(document).ready(function() {
  $(".dropdown_nav__btn").on( "click", function(){
    $('.dropdown_nav').show();
  });
});


// if slick 
if (typeof $(this).slick == 'function') {
  
  var photo_report__slider = $('.gallery_preview').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    dots: true,
    arrows: false,
    autoplaySpeed: 5000,
  });

  $(".slider__prev").click(function() {
      photo_report__slider.slick("slickPrev");
  }), $(".slider__next").click(function() {
      photo_report__slider.slick("slickNext");
  });
  
}
// END if slick


// MAGIC
$(document).ready(function() {
  $('#empty_hidden').load( window.location.href + '?hidden=true' + ' #empty_hidden > p, div#hidden' ).removeAttr('id');
});