<?php
  /**
   * Template Name: Блог
   */
  get_header();
?>

  <section class="breadcrumbs">
    <div class="wrapper">
      <div class="container">
        <div class="col" id="path">
          <a href="">
            Главная
          </a>
          <span class="separator">
            &#8250;
          </span>
          <a href="blog">
            Блог
          </a>
          <span class="separator">
            &#8250;
          </span>
          <span>
            <?php the_title(); ?>
          </span>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="wrapper">


      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


    <div class="blog__item">
      <h2 class="blog__item__title"><?php the_title(); ?></h2>
      <?php the_content(); ?>
    </div>



      <?php endwhile; else: ?>
      <p><?php _e('К сожалению, по вашему запросу ничего не найдено.'); ?></p>
      <?php endif; ?>


    </div>
  </section>

<?php get_footer(); ?>